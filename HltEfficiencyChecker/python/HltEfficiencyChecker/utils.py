###############################################################################
# (c) Copyright 2019-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Define the units and axis titles that the tool knows for plotting
# Make sure that none of the keys are substrings of any other keys, or the logic in hlt_line_efficiencies.py will break.
units = {"PT": "MeV", "TAU": "ns", "P_E": "MeV", "ETA": None}
var_latex_var_map = {
    "PT": r"p_{T}",
    "TAU": r"#tau",
    "P_E": r"E",
    "ETA": r"#eta"
}


def isHlt1(decision):
    if decision.endswith("TrueSim"):
        return "Hlt1" in decision
    else:
        return decision.startswith("Hlt1")


def isHlt2(decision):
    if decision.endswith("TrueSim"):
        return "Hlt2" in decision
    else:
        return decision.startswith("Hlt2")


def getHistoName(denom, decision, suffix):
    return f"{denom}__{decision}__{suffix}"


def getHistoNameBits(name):
    return name.split('__')


def getDenomFromHistoName(name):
    return getHistoNameBits(name)[0]


def getDecisionFromHistoName(name):
    return getHistoNameBits(name)[1]


def make_cut_string(cut_list):
    return ' && '.join([('(' + cut + ')') for cut in cut_list])


def sanitise_line(line):
    new_line_name = str(line)
    for ugly in ['Hlt1', 'Line', 'Decision']:
        new_line_name = new_line_name.replace(ugly, '')
    return new_line_name


def dot_aligned(dicto):
    snums = {k: str(n) for k, n in dicto.items()}
    dots = {k: s.find('.') for k, s in snums.items()}
    m = max(dots.values())
    return {k: ' ' * (m - dots[k]) + snums[k] for k in dicto.keys()}


def get_denoms(parent_name, child_names):

    efficiency_denominators = {
        "AllEvents": "1",
    }

    # Use make_cut_string to append denominators
    efficiency_denominators["CanRecoChildren"] = make_cut_string([
        '{0}_TRUEETA > 2 && {0}_TRUEETA < 5 && {0}_MC_RECONSTRUCTIBLE == 2 '.
        format(kid) for kid in child_names
    ])  # 2 for Charged Long tracks: see functorcollections.MCReconstructible
    efficiency_denominators[
        "CanRecoChildrenParentCut"] = efficiency_denominators[
            "CanRecoChildren"] + ' && ' + make_cut_string([
                parent_name + '_TRUETAU > 0.0002',
                parent_name + '_TRUEPT > 2000'
            ])
    efficiency_denominators['CanRecoChildrenAndChildPt'] = efficiency_denominators['CanRecoChildren'] + ' && ' + \
                make_cut_string(['{0}_TRUEPT > 250'.format(kid) for kid in child_names])

    return efficiency_denominators
