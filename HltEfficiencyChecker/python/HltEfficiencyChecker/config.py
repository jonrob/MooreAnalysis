###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import json
import re
from enum import Enum
from GaudiConf.LbExec import HltSourceID
from PyConf.application import configure_input, configure
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.application import all_nodes_and_algs
from Moore.config import (moore_control_flow, allen_control_flow,
                          get_allen_hlt1_decision_ids)
from PyConf.components import unique_name_ext_re
from RecoConf.mc_checking import make_links_lhcbids_mcparticles_tracking_system
from RecoConf.data_from_file import make_mc_track_info
from RecoConf.hlt1_allen import make_allen_dec_reports

import Functors as F
from FunTuple import FunctorCollection
from FunTuple import functorcollections as FC
from FunTuple import FunTuple_MCParticles as MCFunTuple
from FunTuple import FunTuple_Event as EventFunTuple
from RecoConf.data_from_file import mc_unpackers
from DaVinciMCTools import MCReconstructible as MCRectible

stages = Enum("stage", "Hlt1 Hlt2 Both")


def _get_streams(options):
    streams = options.lines_maker()
    #If streams is a list create dict {default : `DecisionLine` objects}
    if not isinstance(streams, dict): streams = dict(default=streams)
    return streams


def _moore_decision_names(dec_reps_maker):
    # TODO make the decision names properties of ExecutionReportsWriter
    #      e.g. Persist is a map "cf node name" -> "decision name"
    #      and then we don't have the knowledge of the implicit
    #      "Decision" suffix here.
    return [name + "Decision" for name in dec_reps_maker.properties["Persist"]]


def _dump_metadata(options, descriptor_template):
    assert options.ntuple_file, "You must set options.ntuple_file"

    if descriptor_template:
        with open(options.ntuple_file + ".metadata.json", "w") as f:
            json.dump({"descriptor_template": descriptor_template}, f)


def _find_alg_by_name(nodes_or_algs, name):
    pattern = re.compile(name + unique_name_ext_re())
    matches = [n for n in nodes_or_algs if pattern.fullmatch(n.name)]
    if not matches:
        raise ValueError(f"No node/alg named {name}")
    elif len(matches) > 1:
        raise ValueError(f"More than one node/alg named {name}")
    return matches[0]


def parse_descriptor_template(template):
    """Return decay descriptor and branches from a template.

    This function takes a template decay descriptor, e.g.
    ${Dstar}[D*(2010)+ -> ${D0}(D0 -> ${Kminus}K- ${Kplus}K+) ${pisoft}pi+]CC
    and returns a decay descriptor and a dictionary map of branch names
    to their branch-specific decay descriptors. Allows you to specify
    both the expected branch names and the decay descriptor in one go.

    Taken from https://gitlab.cern.ch/snippets/147

    Returns:
        tuple: (decay descriptor, branches, parent).

    """
    from string import Template
    # The argument 'template' is a Python string template
    # e.g. "[${D}D0 -> ${kaon}K- ${pion}pi+]CC"
    # Here ["D", "kaon", "pion"] are the branch names you want
    dd = Template(template)

    # This parses the temlate to get the list of branch names,
    # i.e. ["D", "kaon", "pion"]
    particles = [
        y[1] if len(y[1]) else y[2] for y in dd.pattern.findall(dd.template)
        if len(y[1]) or len(y[2])
    ]
    parent = particles[
        0]  # TODO is this always true - test with a few decay descriptors.

    # To form the decay descriptor, we need to mark all the particles
    # except for the top-level particle
    mapping = {p: '^' if particles.index(p) != 0 else '' for p in particles}
    clean = dd.template.replace(' ', '')
    for i, o in enumerate(re.findall("(\[\$|\$)", clean)):
        if o == '[$': mapping[particles[i]] = ''

    # Make the descriptor
    # "[D0 -> ^K- ^pi+]CC"
    decay_desc = dd.substitute(mapping)

    # Now make the branches
    branches = {}
    for p in particles:
        # Need a version of the descriptor where particle 'p' is marked but nothing else is.
        # Use mapping to ensure the parent particle is never marked.
        branches[p] = dd.substitute(
            {q: mapping[p] if p == q else ''
             for q in particles})

    return decay_desc, branches, parent


def configure_funtuple(stage,
                       hlt1_decision_names=[],
                       hlt2_decision_names=[],
                       hlt1_dec_reports=None,
                       hlt2_dec_reports=None,
                       hlt1_sel_reports=None,
                       hlt2_lines=[],
                       descriptor_template=None):
    """Build FunTuple instance for tupling

    Args that need explaining:
        stage (stages Enum): enum that can be Hlt1, Hlt2 or Both,
        descriptor_template (str): Annotated decay descriptor to be
            in the format of `setDescriptorTemplate`.

    Returns:
        FunTuple instance to put into the control flow.

    """
    efficiency_mode = bool(descriptor_template)
    decision_vars = FunctorCollection({})
    if stage == stages.Hlt1 or stage == stages.Both:
        decision_vars += FunctorCollection({
            dec: F.DECISION(hlt1_dec_reports, dec)
            for dec in hlt1_decision_names
        })
    if stage == stages.Hlt2 or stage == stages.Both:
        decision_vars += FunctorCollection({
            dec: F.DECISION(hlt2_dec_reports, dec)
            for dec in hlt2_decision_names
        })

    if efficiency_mode:

        mckin = FunctorCollection({
            "TRUEM": F.MASS,
            "TRUEP": F.P,
            "TRUEPT": F.PT,
            "TRUEPX": F.PX,
            "TRUEPY": F.PY,
            "TRUEPZ": F.PZ,
            "TRUEETA": F.ETA,
            "TRUEENERGY": F.ENERGY,
            "TRUETAU": F.MC_LIFETIME
        })
        vars_rtible = FC.MCReconstructible(
            mcreconstructible_alg=MCRectible(
                input_mctrackinfo=make_mc_track_info()))

        mc_particles = mc_unpackers()["MCParticles"]
        vars_truesimeff = FunctorCollection({})
        # TODO restore TrueSim efficiencies for HLT1. See MooreAnalysis#41
        # if stage == stages.Hlt1 or stage == stages.Both:
        #     vars_truesimeff += FC._HltMCTrueSimEff(
        #         selection_type=HltSourceID.Hlt1,
        #         trigger_lines=hlt1_decision_names,
        #         mcp_data=mc_particles,
        #         dec_reports=hlt1_dec_reports,
        #         mc2IDLink=make_links_lhcbids_mcparticles_tracking_system(),
        #         sel_reports=hlt1_sel_reports)
        if stage == stages.Hlt2 or stage == stages.Both:
            vars_truesimeff += FC._HltMCTrueSimEff(
                selection_type=HltSourceID.Hlt2,
                trigger_lines=hlt2_decision_names,
                mcp_data=mc_particles,
                dec_reports=hlt2_dec_reports,
                mc2IDLink=make_links_lhcbids_mcparticles_tracking_system(),
                cand_locations=[
                    line.objects_to_persist[0] for line in hlt2_lines
                ])

        _, fields, _ = parse_descriptor_template(descriptor_template)
        return MCFunTuple(
            "MCFunTuple",
            "MCDecayTree",
            fields=fields,
            variables=dict(ALL=mckin + vars_rtible + vars_truesimeff),
            event_variables=decision_vars,
            inputs=mc_particles)
    else:
        return EventFunTuple(
            "EventFunTuple",
            "EventTree",
            variables=decision_vars,
            store_run_event_numbers=False)


def run_allen_in_moore_with_tuples(options,
                                   descriptor_template=None,
                                   public_tools=[]):
    """Run Allen-in-Moore with FunTuple-ing included

    Args:
        options (ApplicationOptions): top-level application options
        descriptor_template (str): Annotated decay descriptor to be
            in the format of `setDescriptorTemplate`.
        public_tools (list): list of public `Tool` instances to configure

    Returns:
        ComponentConfig: updated configuration
    """
    _dump_metadata(options, descriptor_template)

    config = configure_input(options)

    funtuple = configure_funtuple(
        stage=stages.Hlt1,
        hlt1_decision_names=list(get_allen_hlt1_decision_ids().keys()),
        hlt1_dec_reports=make_allen_dec_reports(),
        descriptor_template=descriptor_template)

    top_cf_node = CompositeNode(
        'hlt1_with_efficiency_tuples',
        combine_logic=NodeLogic.NONLAZY_AND,
        children=[allen_control_flow(options), funtuple],
        force_order=True)
    config.update(configure(options, top_cf_node, public_tools=public_tools))
    return config


def run_moore_with_tuples(options,
                          hlt1,
                          descriptor_template=None,
                          public_tools=[]):
    """Run Moore with efficiency/rate tuple makers from FunTuple included

    Args:
        options (ApplicationOptions): top-level application options
        hlt1 (bool): run HLT1 or HLT2
        descriptor_template (str): Annotated decay descriptor to be
            in the format of `setDescriptorTemplate`.
        public_tools (list): list of public `Tool` instances to configure

    Returns:
        ComponentConfig: updated configuration

    """
    _dump_metadata(options, descriptor_template)

    config = configure_input(options)
    streams = _get_streams(options)

    stage = stages.Hlt1 if hlt1 else stages.Hlt2
    moore_cf_node = moore_control_flow(options, streams, stage.name.lower())
    # Make sure that Hlt__RoutingBitsWriter is called for every event
    # TODO add a "flagging mode" option to be used in moore_control_flow
    moore_cf_node.combine_logic = NodeLogic.NONLAZY_AND

    # Get the creator of HltDecReports
    moore_cf_algs = all_nodes_and_algs(moore_cf_node)[1]
    dec_reports_maker = _find_alg_by_name(moore_cf_algs,
                                          "ExecutionReportsWriter")
    decision_names = _moore_decision_names(dec_reports_maker)

    if stage == stages.Hlt1:
        funtuple = configure_funtuple(
            stage,
            hlt1_decision_names=decision_names,
            hlt1_dec_reports=dec_reports_maker.DecReportsLocation,
            descriptor_template=descriptor_template)
    else:
        funtuple = configure_funtuple(
            stage,
            hlt2_decision_names=decision_names,
            hlt2_dec_reports=dec_reports_maker.DecReportsLocation,
            hlt2_lines=streams["default"],
            descriptor_template=descriptor_template)

    top_cf_node = CompositeNode(
        'moore_with_efficiency_tuples',
        combine_logic=NodeLogic.NONLAZY_AND,
        children=[moore_cf_node, funtuple],
        force_order=True)
    config.update(configure(options, top_cf_node, public_tools=public_tools))
    return config


def run_chained_hlt_with_tuples(options,
                                descriptor_template=None,
                                public_tools=[]):
    """Run Allen-in-Moore HLT1 and Moore HLT2 with FunTuple-ing included

    Args:
        options (ApplicationOptions): top-level application options
        descriptor_template (str): Annotated decay descriptor to be
            in the format of `setDescriptorTemplate`.
        public_tools (list): list of public `Tool` instances to configure

    Returns:
        ComponentConfig: updated configuration
    """
    _dump_metadata(options, descriptor_template)

    config = configure_input(options)

    hlt2_streams = _get_streams(options)
    hlt2_node = moore_control_flow(options, hlt2_streams, "hlt2")
    hlt2_node.combine_logic = NodeLogic.NONLAZY_AND

    hlt2_node_algs = all_nodes_and_algs(hlt2_node)[1]
    hlt2_dec_reports_maker = _find_alg_by_name(hlt2_node_algs,
                                               "ExecutionReportsWriter")
    hlt2_decision_names = _moore_decision_names(hlt2_dec_reports_maker)

    funtuple = configure_funtuple(
        stages.Both,
        hlt1_decision_names=list(get_allen_hlt1_decision_ids().keys()),
        hlt2_decision_names=hlt2_decision_names,
        hlt1_dec_reports=make_allen_dec_reports(),
        hlt2_dec_reports=hlt2_dec_reports_maker.DecReportsLocation,
        hlt2_lines=hlt2_streams["default"],
        descriptor_template=descriptor_template)

    top_cf_node = CompositeNode(
        'hlt1_hlt2_with_efficiency_tuples',
        combine_logic=NodeLogic.NONLAZY_AND,
        children=[allen_control_flow(options), hlt2_node, funtuple],
        force_order=True)
    config.update(configure(options, top_cf_node, public_tools=public_tools))
    return config
