#!/usr/bin/env python
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import argparse
import json
import os
import ROOT
import sys
from itertools import product
from HltEfficiencyChecker.config import parse_descriptor_template
import HltEfficiencyChecker.utils as utils
from PyConf.components import findRootObjByDir

ROOT.gROOT.SetBatch(True)
ROOT.gROOT.ProcessLine(f'.x {os.path.dirname(__file__)}/lhcbStyle.C')
ROOT.gStyle.SetLabelSize(0.06, "xy")
ROOT.gStyle.SetTitleOffset(0.8, "y")
ROOT.gStyle.SetTitleOffset(1.1, "x")
ROOT.gStyle.SetNdivisions(507, "y")
ROOT.gStyle.SetNdivisions(507, "x")
ROOT.gStyle.SetLegendTextSize(0.04)
ROOT.gStyle.SetPadTopMargin(0.05)
ROOT.gStyle.SetPadLeftMargin(0.12)
ROOT.gStyle.SetPadBottomMargin(0.15)
ROOT.gStyle.SetPadRightMargin(0.05)


def get_parser():
    parser = argparse.ArgumentParser(
        description=
        "Analysis script to calculate and plot efficiencies from the tuple provided. Use --help for options or visit the Moore online documentation."
    )
    parser.add_argument('input', help='Input ROOT file with tuples.')
    parser.add_argument(
        '--metadata',
        help='JSON input metadata (defaults to <input>.metadata.json)')
    parser.add_argument(
        '--denoms',
        type=str,
        default='CanRecoChildren',
        help='Comma-separated list of efficiency denominators.')
    parser.add_argument(
        '--lines',
        type=str,
        default='',
        help='Comma-separated list of lines to plot results of.')
    parser.add_argument(
        '--vars',
        type=str,
        default='PT',
        help=
        'Comma-separated list of vars to plot against. Defaults to the parent. Specify a particle by prefixing with the branch name and a colon e.g. \"B_s0:PT\"'
    )
    parser.add_argument(
        '--decay-name', type=str, help='Decay short name used in titles.')
    parser.add_argument('--parent')
    parser.add_argument(
        '--reconstructible-children',
        required=True,
        help='Comma-separated list of child names.')
    parser.add_argument(
        '--true-signal-to-match-to',
        type=str,
        default=None,
        help=
        'Comma-separated list of particle names (specified in the annotated decay descriptor) that you\'d like to see TrueSim efficiences for.'
    )
    parser.add_argument(
        '--effs-groups',
        type=str,
        nargs='+',
        default=[],
        help=
        "Whitespace-separated list of groups of lines for which you\'d like to see the inclusive efficiency of, each prefixed with a name and a colon e.g. dimuon_lines:Hlt1DiMuonHighMassDecision,Hlt1DiMuonLowMassDecision."
    )
    parser.add_argument(
        '--plot-group-effs',
        action='store_true',
        help="Plot the group efficiencies as well.")
    parser.add_argument(
        '--custom-denoms',
        type=str,
        default=None,
        help=
        'Comma-separated list of custom cut strings to be applied in addition to the other denominators. E.g. you could pass "Hlt1TrackMVADecision || Hlt1TwoTrackMVADecision" to assess an Hlt2 efficiency with respect to Hlt1. You can also pass each custom denominator a nickname by passing a name with a colon e.g "Hlt1TrackMVAs:Hlt1TrackMVADecision || Hlt1TwoTrackMVADecision"'
    )
    parser.add_argument('--make-plots', action='store_true')
    parser.add_argument(
        '--legend-header',
        help=
        'Give your plot a nice header. TLatex is supported e.g. J/#psi #rightarrow #mu#mu'
    )
    parser.add_argument(
        '--xtitle',
        help=
        'Optional plot x-axis title. TLatex supported e.g. \"p_{T} (B^{0}_{s})\"'
    )
    parser.add_argument('--plot-format', default='pdf')
    parser.add_argument(
        '--plot-prefix', default='', help='Prefix for the plot file paths.')
    parser.add_argument(
        '--shape-distr-with-denom',
        default='',
        help=
        'For plots where more than 1 denom is shown, specify the denom cut to apply on the shape histogram shown in the background of the plot. For plots with 1 denom only, that denom is used'
    )
    parser.add_argument(
        '--nbins', type=int, default=20, help="Number of efficiency bins.")
    parser.add_argument(
        '--shape-nbins',
        type=int,
        default=100,
        help=
        "Number of bins the underlying variable distribution will be plotted in."
    )
    parser.add_argument('--no-legend', action='store_true')
    parser.add_argument(
        '--json', type=str, help='If set to a filename, write results as json')
    return parser


def main():
    parser = get_parser()
    args = parser.parse_args()

    print('-' * 84)
    print(f'INFO:\t Starting {os.path.realpath(__file__)}...')
    print('-' * 84)

    # Handle args that don't make sense
    vars_with_no_unit = []
    for var in args.vars.split(','):
        if not [k for k in utils.units.keys() if k in var]:
            print(
                f"WARNING:\t Don't know the units of variable \"{var}\", so won\'t put units on the x-axis title unless you use --xtitle to specify it yourself."
            )
            vars_with_no_unit.append(var)

    metadata_path = args.metadata or (f'{args.input}.metadata.json')
    metadata = {}
    if os.path.isfile(metadata_path):
        with open(metadata_path) as f:
            metadata = json.load(f)
    input_descriptor = None
    try:
        input_descriptor = parse_descriptor_template(
            metadata['descriptor_template'])
    except KeyError:
        pass

    if not args.parent:
        if input_descriptor:
            args.parent = input_descriptor[2]
        else:
            parser.error(
                f'--parent cannot be guessed from metadata ({metadata_path}), give '
                'it explicitly')

    args.reconstructible_children = args.reconstructible_children.split(',')
    if input_descriptor:
        missing_children = set(args.reconstructible_children).difference(
            input_descriptor[1])
        if missing_children:
            parser.error(
                f'reconstructible_children {missing_children} are not in input'
            )

    if input_descriptor and not args.decay_name:
        args.decay_name = input_descriptor[0]

    # Define the denominator cuts
    parent_name = args.parent
    child_names = args.reconstructible_children

    # Give unique names to the custom denominators is any are passed
    custom_denoms = {}
    if args.custom_denoms is not None:
        for i, custom_denom in enumerate(args.custom_denoms.split(',')):
            if ':' in custom_denom:
                try:
                    nickname = custom_denom.split(':')[0]
                    custom_cut = custom_denom.split(':')[1]
                except AttributeError:
                    print("FATAL:\t Could not disentangle custom denominator",
                          custom_denom)
                    sys.exit(1)
            else:
                nickname = f'CustomDenom{i+1}'
                custom_cut = custom_denom

            custom_denoms[nickname] = custom_cut

    # Select the denominators that were parsed from the dict of defined denominators
    denoms = {}
    known_denoms = utils.get_denoms(parent_name, child_names)
    for key in args.denoms.split(','):
        if key in known_denoms.keys():
            denoms[key] = known_denoms[key]
            # add any custom denominators also
            for custom_name, custom_cut in custom_denoms.items():
                custom_key = f'{key}And{custom_name}'
                # wrap custom cut in brackets in case it contains and OR / ||
                denoms[custom_key] = ' && '.join(
                    [known_denoms[key], f'({custom_cut})'])
        else:
            print(
                f"WARNING:\t Parsed denominator \"{key}\" is not implemented. Skipping this denominator."
            )

    if not denoms:
        print(
            "WARNING:\t None of the denominators specified are implemented. Defaulting to \"CanRecoChildren\" denominator..."
        )
        denoms["CanRecoChildren"] = known_denoms["CanRecoChildren"]

    if not args.shape_distr_with_denom:
        if len(denoms) > 1:
            print(
                "WARNING:\t More than 1 denominators specified, but --shape-distr-with-denom unspecified."
            )
            print(
                "WARNING:\t Will draw the variable shape histos subject to the \"CanRecoChildren\" denominator."
            )
            denoms["CanRecoChildren"] = known_denoms["CanRecoChildren"]
            shape_hist_denom = "CanRecoChildren"
        else:
            shape_hist_denom = list(denoms.keys())[0]
    else:
        if args.shape_distr_with_denom not in denoms.keys():
            print(
                f"WARNING:\t Don\'t know the denominator \"{args.shape_distr_with_denom}\" specified for the shape distributions!"
            )
            print(
                "WARNING:\t Will draw the variable shape histos subject to the \"CanRecoChildren\" denominator."
            )
            denoms["CanRecoChildren"] = known_denoms["CanRecoChildren"]
            shape_hist_denom = "CanRecoChildren"
        else:
            shape_hist_denom = args.shape_distr_with_denom

    # Now find all the trigger decisions from the tree, and from this only analyse those from lines that the user asked for in args.lines
    # By default, analyse all the decisions in the tree if --lines is not specified

    tuple_file = ROOT.TFile(args.input, 'Read')
    tree = findRootObjByDir(tuple_file, "MCFunTuple", "MCDecayTree")
    try:
        decisions_in_tree = [
            br.GetName() for br in tree.GetListOfBranches()
            if ((br.GetName().startswith("Hlt1") or br.GetName().startswith(
                "Hlt2")) and br.GetName().endswith("Decision"))
        ]
        true_sim_decisions_in_tree = [
            br.GetName() for br in tree.GetListOfBranches()
            if (("Hlt1" in br.GetName() or "Hlt2" in br.GetName())
                and br.GetName().endswith("DecisionTrueSim"))
        ]
    except AttributeError:
        print(
            "FATAL:\t Seems there are no branches in the tree. Was the tuple created correctly and without failure?"
        )
        sys.exit(1)

    decisions = []
    if args.lines:
        for line in args.lines.split(','):
            if line in decisions_in_tree:
                decisions.append(line)
            else:
                print(
                    f"WARNING:\t Parsed line {line} has not been implemented/run on this sample. Skipping this line."
                )
    else:
        print("INFO:\t No lines specified. Defaulting to all...")
        decisions = decisions_in_tree
    if decisions == []:
        print(
            "WARNING:\t None of the lines specified are implemented. Defaulting to all lines that are present."
        )
        decisions = decisions_in_tree

    # Search for definitions of groups of lines
    groups = {}
    if args.effs_groups:
        for effs_group in args.effs_groups:
            name, comma_separated_lines = effs_group.split(':')
            groups[name] = comma_separated_lines.split(',')
        for name, group in groups.items():
            if not (all([utils.isHlt1(dec) for dec in group])
                    or all([utils.isHlt2(dec) for dec in group])):
                raise RuntimeError(
                    f"Can\'t calculate an inclusive efficiency for group {name} as it appears to contain lines from HLT1 and HLT2.\n Lines in {name}: {group}"
                )

    # FIXME upgrade groups to use intags and outtags like overlap checker.
    # Then remove these defaults
    # Don't add the default groups unless user has asked for some groups
    default_groups = {}
    default_groups['Hlt1TrackMVA'] = [
        dec for dec in decisions
        if 'Track' in dec and 'MVA' in dec and utils.isHlt1(dec)
    ]
    default_groups['Hlt1Muon'] = [
        dec for dec in decisions if 'Muon' in dec and utils.isHlt1(dec)
    ]
    default_groups['Hlt1Charm'] = [
        dec for dec in decisions if 'D2' in dec and utils.isHlt1(dec)
    ]
    all_hlt1_physics_decisions = default_groups[
        'Hlt1TrackMVA'] + default_groups["Hlt1Muon"] + default_groups[
            "Hlt1Charm"]
    default_groups['Hlt1NotPhysics'] = [
        dec for dec in decisions
        if dec not in all_hlt1_physics_decisions and utils.isHlt1(dec)
    ]
    for name, group in default_groups.items():
        if group:
            groups[name] = group
    #groups contains both default and user specified groups

    # Now add the TrueSim efficiencies that were requested and create TrueSimEff groups
    true_sim_decisions = []
    true_sim_decision_groups = {}
    if not args.true_signal_to_match_to:
        print(
            f"INFO:\t Which particles to show TrueSim efficiencies for was unspecified. Defaulting to parent ({args.parent})..."
        )
        args.true_signal_to_match_to = args.parent

    for particle_pfx, dec in product(
            args.true_signal_to_match_to.split(','), decisions):
        truesimeff_name = f"{particle_pfx}_{dec}TrueSim"
        if truesimeff_name in true_sim_decisions_in_tree:
            true_sim_decisions.append(truesimeff_name)
        else:
            print(
                f"WARNING:\t Parsed TrueSim decision {truesimeff_name} does not have a branch in the tree. Skipping this one."
            )

    if args.effs_groups:
        for name, group in groups.items():
            tse_in_group = []
            for particle_pfx, dec in product(
                    args.true_signal_to_match_to.split(','), group):
                truesimeff_name = f"{particle_pfx}_{dec}TrueSim"
                if truesimeff_name in true_sim_decisions_in_tree:
                    tse_in_group.append(truesimeff_name)
            true_sim_decision_groups[f'{name}TrueSim'] = tse_in_group

        for name, group in true_sim_decision_groups.items():
            if group:
                groups[name] = group

    decisions += true_sim_decisions
    length = max([len(dec) for dec in decisions])  # for formatting

    # Split the decisions into TrueSim and event decisions for ease later
    evt_decisions = [dec for dec in decisions if "TrueSim" not in dec]
    true_sim_decisions = [dec for dec in decisions if "TrueSim" in dec]
    all_decisions = list(set(evt_decisions + true_sim_decisions))
    if all_decisions.sort() != decisions.sort():
        raise RuntimeError(
            f"FATAL:\tThe set of TrueSim and event decisions is {all_decisions} but the total list of decisions is {decisions}"
        )

    # Now calculate some integrated efficiencies and make some plots
    integrated_efficiencies = {denom: {} for denom in denoms.keys()}
    integrated_efficiencies_err = {denom: {} for denom in denoms.keys()}

    bad_denom_keys = []
    for denom, cut in denoms.items():
        # Get the number of evts with positive decisions for each denominator
        denom_evts = float(tree.GetEntries(cut))
        if not denom_evts:
            print(
                f"WARNING:\t Denominator {denom} gives no events. Skipping this denominator."
            )
            bad_denom_keys.append(denom)
            del integrated_efficiencies[denom]
            del integrated_efficiencies_err[denom]
            continue

        print(
            f'Integrated HLT efficiencies for the lines with denominator: {denom}'
        )
        print('-' * 84)

        for dec in decisions:
            cut_str = ' && '.join(filter(None, [cut, dec]))
            eff = float(tree.GetEntries(cut_str)) / denom_evts
            integrated_efficiencies[denom][dec] = eff
            eff_bin_err = ROOT.TMath.Sqrt(eff * (1.0 - eff) / denom_evts)
            integrated_efficiencies_err[denom][dec] = eff_bin_err
            print(
                "Line:\t {0:<{1}}\t Efficiency:\t {2:.3f} +/- {3:.3f}".format(
                    dec, length, eff, eff_bin_err))

        if args.effs_groups:
            for name, group in groups.items():
                print('-' * 84)
                incl_selection = f"({cut}) && ({' || '.join(filter(None, group))})"
                incl_group_eff = float(
                    tree.GetEntries(incl_selection)) / denom_evts
                integrated_efficiencies[denom][name] = incl_group_eff
                incl_group_eff_err = ROOT.TMath.Sqrt(
                    incl_group_eff * (1.0 - incl_group_eff) / denom_evts)
                integrated_efficiencies_err[denom][name] = incl_group_eff_err

            print('Efficiencies grouped by common physics: ')
            for name, decs_in_group in groups.items():
                print(f'INFO:\t {name} group contains {decs_in_group}.')
            print('-' * 84)
            for name in groups.keys():
                print("Group:\t {0:<{1}}\t Efficiency: \t {2:.3f} +/- {3:.3f}".
                      format(f'{name} lines', length,
                             integrated_efficiencies[denom][name],
                             integrated_efficiencies_err[denom][name]))
            print('-' * 84)
        print('-' * 84)
        print(
            f'Finished printing integrated HLT efficiencies for denominator: {denom}'
        )
        print('-' * 84)

    if args.json:
        with open(args.json, 'w') as f:
            json.dump(
                {
                    'integrated_efficiencies': integrated_efficiencies,
                    'integrated_efficiencies_errors':
                    integrated_efficiencies_err
                },
                f,
                indent=4)
        print(f"INFO:\t Written results to {args.json}.")

    if not args.make_plots:
        # FIXME partition the below off into another function
        sys.exit(0)  # Success

    ROOT.TH1.SetDefaultSumw2()
    eff_plot_ymax = 1.1
    eff_plot_ymin = 0.0
    print('-' * 84)
    print('INFO:\t Making plots...')
    print('-' * 84)

    if args.no_legend:
        print(
            "INFO:\t --no-legend used. Instead printing the (colour, marker) of each line..."
        )
    else:
        if not args.legend_header:
            print(
                "INFO:\t No --legend-header specified. Using the config file name as a legend header instead..."
            )

    # Clean out the denominators for which no events are retained
    for bad in bad_denom_keys:
        del denoms[bad]
        if not denoms:
            raise RuntimeError(
                "FATAL:\t No remaining denominators. Please specify some denominators that retain events."
            )
        if bad == shape_hist_denom:
            raise RuntimeError(
                f"FATAL:\t Denominator \"{shape_hist_denom}\" chosen for shape distributions gives no events. Please choose another denominator with --shape-distr-with-denom <denom>."
            )

    markers = {
        "open stars": ROOT.kOpenStar,
        "stars": ROOT.kStar,
        "circles": ROOT.kCircle,
        "upward triangles": ROOT.kOpenTriangleUp,
        "downward triangles": ROOT.kOpenTriangleDown
    }
    colours = {
        "red": ROOT.kRed,
        "green": ROOT.kGreen + 2,
        "blue": ROOT.kBlue,
        "orange": ROOT.kOrange + 1,
        "magenta": ROOT.kMagenta,
        "black": ROOT.kBlack
    }

    def print_colour_and_marker(histo):
        found_mark = False
        found_col = False
        for k, v in markers.items():
            if v == histo.GetMarkerStyle():
                mark = k
                found_mark = True
        for k, v in colours.items():
            if v == histo.GetLineColor():
                col = k
                found_col = True
        if not (found_mark and found_col):
            raise RuntimeError(
                "FATAL:\t Couldn\'t find colour and/or marker of this histo. Something has gone wrong."
            )
        name = histo.GetName()
        print(
            f"INFO:\t The {col} line with {mark} is the {utils.getDecisionFromHistoName(name)} line with the {utils.getDenomFromHistoName(name)} denom."
        )

    for var in args.vars.split(','):
        #---------- Histogram making ----------------------
        eff_histos = {}

        if ':' in var:
            selected_particle, var_sfx = var.split(':')
            draw_var = f"{selected_particle}_TRUE{var_sfx}"
        else:
            # Assume parent kinematics wanted
            selected_particle = parent_name
            draw_var = f"{selected_particle}_TRUE{var}"

        # Need to get sensible x limits based on the shape of this var's distribution,
        # subject to the denominator cut. Draw the shape histo, calculate mean and 3sigma width,
        # then make that the upper limit.
        xmin = tree.GetMinimum(draw_var)
        limit_hist = ROOT.TH1F(f"{draw_var}_limhist", "", args.shape_nbins,
                               xmin, tree.GetMaximum(draw_var))
        tree.Draw(f"{draw_var}>>{draw_var}_limhist", denoms[shape_hist_denom])
        limit_hist.SetDirectory(0)
        xmax = float(limit_hist.GetMean() + (3 * limit_hist.GetStdDev()))

        if args.xtitle:
            xtitle = args.xtitle
        else:
            print(
                "INFO:\t --xtitle not specified. Using branch name and attempting to figure out the units..."
            )
            if var in vars_with_no_unit:
                print(
                    f"WARNING:\t No unit or latex title known for variable \"{var}\", writing variable as-is with no unit..."
                )
                xtitle = draw_var
            else:
                known_latex_var, unit = [
                    (k, v) for k, v in utils.units.items() if k in var
                ][0]  # Assume it picks out only 1 unit from the dictionary.
                print(f"INFO:\t Found unit \"{unit}\" for variable \"{var}\".")
                unit_str = f" / {unit}" if unit else ""
                xtitle = f"{utils.var_latex_var_map[known_latex_var]}({selected_particle}){unit_str}"

        selections = {dec: dec for dec in decisions}
        if args.effs_groups:
            group_selections = {
                name: f"({' && '.join(group)})"
                for name, group in groups.items()
            }
            selections = {**selections, **group_selections}

        for i, denom_key in enumerate(sorted(denoms)):
            for j, (name, selection) in enumerate(selections.items()):
                # --------- Histo drawing ------------------

                pass_cuts = ' && '.join(
                    filter(None, [denoms[denom_key], selection]))
                passhist = ROOT.TH1F(
                    utils.getHistoName(denom_key, name, "pass"),
                    utils.getHistoName(denom_key, name, "pass"), args.nbins,
                    xmin, xmax)
                totalhist = passhist.Clone(
                    utils.getHistoName(denom_key, name, "total"))
                effhist = passhist.Clone(
                    utils.getHistoName(denom_key, name, "eff"))

                tree.Draw(
                    f"{draw_var}>>{utils.getHistoName(denom_key, name, 'pass')}",
                    pass_cuts)
                tree.Draw(
                    f"{draw_var}>>{utils.getHistoName(denom_key, name, 'total')}",
                    denoms[denom_key])

                for hist in (passhist, totalhist, effhist):
                    hist.SetDirectory(0)

                effhist.Divide(passhist, totalhist, 1.0, 1.0,
                               "B")  # Using binomial errors for an efficiency

                effhist.SetMarkerStyle(
                    list(markers.values())[(i * len(selections) + j) % len(
                        list(markers.values()))])
                effhist.SetMarkerColor(
                    list(colours.values())[(i * len(selections) + j) % len(
                        list(colours.values()))])
                effhist.SetLineColor(
                    list(colours.values())[(i * len(selections) + j) % len(
                        list(colours.values()))])
                effhist.SetNameTitle(
                    utils.getHistoName(denom_key, name, "eff"), "")
                eff_histos[utils.getHistoName(denom_key, name,
                                              "eff")] = effhist

            # Finally, draw the shape hist subject to this denominator and save to dict
            # Bin the distribution more finely than the efficiencies
            distr = ROOT.TH1F(f"{denom_key}__distr", "", args.shape_nbins,
                              xmin, xmax)
            tree.Draw(f"{draw_var}>>{denom_key}__distr", denoms[denom_key])
            distr.SetDirectory(0)

            # These shape distributions will always control the axes
            # so handle all the axes titles and so on once, now.
            # Also scale to fit in the efficiency y-range.
            oldmax = distr.GetMaximum()
            if oldmax:
                for ibin in range(1, distr.GetNbinsX() + 1):
                    distr.SetBinContent(
                        ibin, eff_plot_ymin +
                        (0.9 * (eff_plot_ymax - eff_plot_ymin) / oldmax) *
                        distr.GetBinContent(ibin))
            distr.SetMinimum(eff_plot_ymin)
            distr.SetMaximum(eff_plot_ymax)
            distr.GetYaxis().SetTitle("Efficiency")
            distr.GetXaxis().SetTitle(xtitle)
            distr.SetFillColorAlpha(ROOT.kGray, 0.50)
            distr.SetLineWidth(0)
            eff_histos[f"{denom_key}__distr"] = distr

        # --------- Plotting ----------------------------
        def plot_legend_and_line_at_one(denoms_on_plot, lines_on_plot,
                                        events_in_plot, shape_denom_key, logx):
            line_at_one = ROOT.TGraph(ROOT.TF1("onefunc", "1.0", xmin, xmax))
            line_at_one.SetLineStyle(ROOT.kDashed)
            line_at_one.SetLineColor(ROOT.kBlack)

            legend = ROOT.TLegend(
                0.13 if logx else 0.53,
                0.82 - (0.1 * len(denoms_on_plot) * len(lines_on_plot)),
                0.53 if logx else 0.93, 0.92)
            if args.legend_header:
                legend_header = args.legend_header
            else:
                legend_header = args.decay_name
            legend.SetHeader(legend_header)
            shape_denom_acronym = '.'.join(
                [letter for letter in shape_denom_key if letter.isupper()])
            for denom_key, dec in product(denoms_on_plot, lines_on_plot):
                short_dec = dec
                for long in ["Hlt1", "Hlt2", "Decision", "Line", "TrueSim"]:
                    short_dec = short_dec.replace(long, "")
                denom_acronym = '.'.join(
                    [letter for letter in denom_key if letter.isupper()])
                legend.AddEntry(
                    eff_histos[utils.getHistoName(denom_key, dec, 'eff')],
                    (f'{denom_acronym}, ' if len(denoms_on_plot) > 1 else "") +
                    f'{short_dec},', 'LP')
                eff_str = f"#varepsilon{' (TrueSim)' if 'TrueSim' in dec else ''}"
                legend.AddEntry(
                    eff_histos[utils.getHistoName(denom_key, dec, 'eff')],
                    eff_str + r' = {0:.2f} #pm  {1:.2f}'.format(
                        integrated_efficiencies[denom_key][dec],
                        integrated_efficiencies_err[denom_key][dec]), '')
            legend.AddEntry(eff_histos[f"{shape_denom_key}__distr"],
                            f"Distribution ({shape_denom_acronym}),", 'F')
            legend.AddEntry(eff_histos[f"{shape_denom_key}__distr"],
                            f"{events_in_plot:.0f} events", '')
            legend.SetFillColorAlpha(ROOT.kWhite, 0.70)
            legend.SetLineWidth(0)

            return line_at_one, legend

        def get_events_in_plot(denom_key):
            cutstring = utils.make_cut_string(
                [denoms[denom_key]] +
                [f"{draw_var} > {xmin}", f"{draw_var} < {xmax}"])
            return tree.GetEntries(cutstring)

        def plot_namer(title, all_lines=False, all_denoms=False):
            var_bit = f"{'log' if logx else ''}{var.replace(':','_')}"
            denom_bit = "AllDenoms" if all_denoms and len(denoms) > 1 else ""
            line_bit = "AllLines" if all_lines and len(
                evt_decisions) > 1 else ""
            bits = '__'.join(filter(None, [line_bit, denom_bit, var_bit]))
            return f"{args.plot_prefix}{title}__{bits}.{args.plot_format}"

        print("INFO:\t Now plotting...")
        if args.lines:
            # If args.lines was specified, we assume the user wants to see a plot of
            # all the specified lines, along with all the specifed denoms, on 1 axis
            for logx in [True, False]:
                c = ROOT.TCanvas()
                c.SetLogx(logx)
                eff_histos[f"{shape_hist_denom}__distr"].Draw("HIST")
                for denom_key, evt_dec in product(denoms.keys(),
                                                  evt_decisions):
                    # Loop over *evt_decisions*, not args.lines, as *evt_decisions* was cleaned of bad lines above
                    this_histo = eff_histos[utils.getHistoName(
                        denom_key, evt_dec, 'eff')]
                    this_histo.Draw("SAME PE0")
                    if args.no_legend: print_colour_and_marker(this_histo)

                line_at_one, legend = plot_legend_and_line_at_one(
                    denoms.keys(), evt_decisions,
                    get_events_in_plot(shape_hist_denom), shape_hist_denom,
                    logx)
                line_at_one.Draw("SAME")
                if not args.no_legend: legend.Draw()
                c.Print(
                    plot_namer(
                        'Efficiencies', all_lines=True, all_denoms=True))

        if args.effs_groups:
            # If args.effs_groups was specified, we assume the user wants to see a plot of
            # all the specified groups, along with all the specifed denoms, on 1 axis
            for logx in [True, False]:
                c = ROOT.TCanvas()
                c.SetLogx(logx)
                eff_histos[f"{shape_hist_denom}__distr"].Draw("HIST")
                for denom_key, name in product(denoms.keys(), groups.keys()):
                    this_histo = eff_histos[utils.getHistoName(
                        denom_key, name, 'eff')]
                    this_histo.Draw("SAME PE0")
                    if args.no_legend: print_colour_and_marker(this_histo)

                line_at_one, legend = plot_legend_and_line_at_one(
                    denoms.keys(), groups.keys(),
                    get_events_in_plot(shape_hist_denom), shape_hist_denom,
                    logx)
                line_at_one.Draw("SAME")
                if not args.no_legend: legend.Draw()
                c.Print(plot_namer('Efficiencies__Groups', all_denoms=True))

        # Need a plot of all TrueSim effs for 1 trigger line, with 1 denominator
        # Either this ends up being every line or what is specified through args.lines
        for evt_dec, logx, denom_key in product(evt_decisions, [True, False],
                                                denoms.keys()):

            true_sim_decs_this_line = list(
                filter(lambda x: evt_dec in x, true_sim_decisions))

            c = ROOT.TCanvas()
            c.SetLogx(logx)
            eff_histos[f"{denom_key}__distr"].Draw("HIST")
            eff_histos[utils.getHistoName(denom_key, evt_dec,
                                          'eff')].Draw("SAME PE0")

            for true_sim_dec in true_sim_decs_this_line:
                eff_histos[utils.getHistoName(denom_key, true_sim_dec,
                                              'eff')].Draw("SAME PE0")

            line_at_one, legend = plot_legend_and_line_at_one(
                [denom_key], [evt_dec] + true_sim_decs_this_line,
                get_events_in_plot(denom_key), denom_key, logx)
            line_at_one.Draw("SAME")
            if not args.no_legend: legend.Draw()
            c.Print(plot_namer(f'Efficiencies__{evt_dec}__{denom_key}'))

        for mcp, denom_key, logx in product(
                args.true_signal_to_match_to.split(','), denoms,
            [True, False]):
            # Plot all TrueSim effs w.r.t. this particle
            true_sim_decisions_this_mcp = [
                true_sim_dec for true_sim_dec in true_sim_decisions
                if true_sim_dec.split("_Hlt")[0] == mcp
            ]
            c = ROOT.TCanvas()
            c.SetLogx(logx)
            eff_histos[f"{denom_key}__distr"].Draw("HIST")

            for true_sim_dec in true_sim_decisions_this_mcp:
                eff_histos[utils.getHistoName(denom_key, true_sim_dec,
                                              'eff')].Draw("SAME PE0")

            line_at_one, legend = plot_legend_and_line_at_one(
                [denom_key], true_sim_decisions_this_mcp,
                get_events_in_plot(denom_key), denom_key, logx)
            line_at_one.Draw("SAME")
            if not args.no_legend: legend.Draw()
            c.Print(
                plot_namer(
                    f'{mcp}__TrueSimEfficiencies__{denom_key}',
                    all_lines=True))

    print('-' * 84)
    print('INFO\t Finished making plots. Goodbye.')
    print('-' * 84)


if __name__ == "__main__":
    main()
