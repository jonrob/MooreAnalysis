#!/usr/bin/env python
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import argparse
import json
import os
import sys
import ROOT
import HltEfficiencyChecker.utils as utils
from PyConf.components import findRootObjByDir

ROOT.gROOT.SetBatch(True)
ROOT.gROOT.ProcessLine('.x {}/lhcbStyle.C'.format(os.path.dirname(__file__)))
ROOT.gStyle.SetOptStat(False)


def get_parser():
    parser = argparse.ArgumentParser(
        description=
        "Analysis script to compute high level trigger rates from a tuple.")
    parser.add_argument('input', help='Input ROOT file with tuples.')
    parser.add_argument(
        '--input-rate',
        type=float,
        default=30000,
        help=
        'Input rate of min. bias sample in kHz. If sample is not HLT1-filtered, should be 30000. If HLT1-filtered, number will vary by sample but be close to 1000 - please see examples.'
    )
    parser.add_argument(
        '--lines',
        type=str,
        default=None,
        help="Comma-separated list of lines to compute rates of.")
    parser.add_argument(
        '--filter-lines',
        type=str,
        default=None,
        help=
        'Comma-separated list of lines that we calculate the rate with respect to (w.r.t.) i.e. at least one of these must pass in every considered event. Intended use case is for rates of a HLT2 line w.r.t passing some HLT1 lines.'
    )
    parser.add_argument(
        '--rates-groups',
        type=str,
        nargs='+',
        help=
        "Whitespace-separated list of groups of lines for which you\'d like to see the inclusive rate of, each prefixed with a name and a colon e.g. dimuon_lines:Hlt1DiMuonHighMassDecision,Hlt1DiMuonLowMassDecision."
    )
    parser.add_argument('--plot-rates', action='store_true')
    parser.add_argument(
        '--plot-total-rate',
        action='store_true',
        help="Plot the total rate inclusive of all lines specified.")
    parser.add_argument(
        '--plot-group-rates',
        action='store_true',
        help="Plot the group rates as well.")
    parser.add_argument('--plot-format', dest='plot_format', default='pdf')
    parser.add_argument(
        '--json', help='If set to a filename, write results as json')
    parser.add_argument('--plot-prefix', default='')
    return parser


def get_level(line):
    if utils.isHlt1(line):
        return "Hlt1"
    elif utils.isHlt2(line):
        return "Hlt2"
    else:
        raise RuntimeError(
            f"FATAL:\t Can\'t figure out trigger level of {line}.")


def main():
    parser = get_parser()
    args = parser.parse_args()

    print('-' * 110)
    print('INFO:\t Starting %s...' % os.path.realpath(__file__))
    print('-' * 110)

    # Now find all the trigger lines from the tree, and from this only analyse those that the user asked for in args.lines
    stats_file = ROOT.TFile(args.input, 'Read')
    tree = findRootObjByDir(stats_file, "EventFunTuple", "EventTree")
    lines_in_tree = [
        br.GetName() for br in tree.GetListOfBranches()
        if ((utils.isHlt1(br.GetName()) or utils.isHlt2(br.GetName()))
            and br.GetName().endswith("Decision"))
    ]
    if not lines_in_tree:
        print("FATAL:\t Didn\'t find any lines in the tree. Aborting...")
        sys.exit()
    lines = []
    if args.lines:
        for key in args.lines.split(','):
            if key in lines_in_tree:
                lines.append(key)
            else:
                print(
                    "WARNING:\t Parsed line " + key +
                    " has not been implemented/run on this sample. Skipping this line."
                )
    else:
        print("INFO:\t No lines specified. Defaulting to all...")
        lines = lines_in_tree
    if lines == []:
        print(
            "WARNING:\t None of the lines specified are implemented. Defaulting to all lines that are present."
        )
        lines = lines_in_tree
    length = max([len(line) for line in lines])  # for formatting
    lines.sort()  # Will put HLT1 first, then HLT2
    hlt1_lines = [line for line in lines if utils.isHlt1(line)]
    hlt2_lines = [line for line in lines if utils.isHlt2(line)]

    # Now calculate some integrated efficiencies and make some plots
    rates = {}
    rates_errors = {}

    filter_cut = ""
    filter_nickname = ""
    if args.filter_lines is not None:
        if args.filter_lines.upper() == "ANYHLT1LINE":
            filter_cut = ' || '.join(hlt1_lines)
            filter_nickname = "AnyHlt1Line"
        else:
            for line in args.filter_lines.split(','):
                if line not in lines_in_tree:
                    raise RuntimeError(
                        f"Can\'t find specified filter line {line} in the tree."
                    )
            filter_cut = ' || '.join(args.filter_lines.split(','))
            filter_nickname = filter_cut
    filter_title = f" w.r.t. passing {filter_nickname}" if filter_nickname else ""

    def get_numer(pass_cut):
        return f"({pass_cut})" + (f" && ({filter_cut})" if filter_cut else "")

    def passed_any_other_line(line):
        lines_this_stage = hlt1_lines if utils.isHlt1(line) else hlt2_lines
        other_lines = set(lines_this_stage).difference({line})
        if len(other_lines) > 0:
            return " || ".join(other_lines)
        else:
            return "1>0"

    # Get the number of evts that trigger for each line
    # Turn into rate by dividing by 30MHz (the LHCb collision rate)
    # We would also like to know the exclusive rate: the rate where the events only fire that trigger.
    denom_evts = float(tree.GetEntries())
    if not denom_evts:
        print("FATAL:\t Tuple seems to be empty. Aborting...")
        sys.exit()

    print(f'HLT rates{filter_title}: ')
    print('-' * 110)
    for line in lines:

        incl_eff = float(tree.GetEntries(get_numer(line))) / denom_evts
        incl_rate = incl_eff * args.input_rate  # in kHz
        excl_eff = float(
            tree.GetEntries(
                get_numer(f'{line} && !({passed_any_other_line(line)})'))
        ) / denom_evts
        excl_rate = excl_eff * args.input_rate
        rates[line] = incl_rate
        rates[line + 'Exclusive'] = excl_rate
        incl_rate_err = ROOT.TMath.Sqrt(
            incl_eff * (1.0 - incl_eff) / denom_evts) * args.input_rate
        excl_rate_err = ROOT.TMath.Sqrt(
            excl_eff * (1.0 - excl_eff) / denom_evts) * args.input_rate
        rates_errors[line] = incl_rate_err
        rates_errors[line + 'Exclusive'] = excl_rate_err

    # Finally, want to print rates for events that fire on the OR of a group of lines,
    # e.g. any muon-related line.
    groups = {}
    if args.rates_groups:
        for rates_group in args.rates_groups:
            name, comma_separated_lines = rates_group.split(':')
            groups[name] = comma_separated_lines.split(',')

    default_groups = {}
    default_groups['Hlt1TrackMVA'] = [
        line for line in hlt1_lines if 'Track' in line and 'MVA' in line
    ]
    default_groups['Hlt1Muon'] = [
        line for line in hlt1_lines if 'Muon' in line
    ]
    default_groups['Hlt1Charm'] = [line for line in hlt1_lines if 'D2' in line]
    all_hlt1_physics_lines = default_groups['Hlt1TrackMVA'] + default_groups[
        "Hlt1Muon"] + default_groups["Hlt1Charm"]
    default_groups['Hlt1NotPhysics'] = [
        line for line in hlt1_lines if line not in all_hlt1_physics_lines
    ]
    for name, group in default_groups.items():
        if group:
            groups[name] = group

    for name, group in groups.items():
        if all([utils.isHlt1(line) for line in group]):
            isThisLevel = utils.isHlt1
        elif all([utils.isHlt2(line) for line in group]):
            isThisLevel = utils.isHlt2
        else:
            print(
                f"WARNING:\t Can\'t calculate an inclusive rate for group \"{name}\" as it appears to contain lines from HLT1 and HLT2."
            )
            print(f"WARNING:\t Lines in {name}: {', '.join(group)}")
            continue
        incl_selection = ' || '.join([line for line in group])
        incl_group_eff = float(tree.GetEntries(
            get_numer(incl_selection))) / denom_evts
        incl_group_rate = incl_group_eff * args.input_rate  # in kHz
        rates[name] = incl_group_rate
        incl_group_rate_err = ROOT.TMath.Sqrt(
            incl_group_eff *
            (1.0 - incl_group_eff) / denom_evts) * args.input_rate
        rates_errors[name] = incl_group_rate_err

        lines_not_in_this_group = [
            line for line in lines if (line not in group and isThisLevel(line))
        ]
        if lines_not_in_this_group:
            excl_selection = f"({incl_selection}) && ({' && '.join([f'!{line}' for line in lines_not_in_this_group])})"
        else:
            excl_selection = incl_selection
        excl_group_eff = float(tree.GetEntries(
            get_numer(excl_selection))) / denom_evts
        excl_group_rate = excl_group_eff * args.input_rate
        rates[f'{name}Exclusive'] = excl_group_rate
        excl_group_rate_err = ROOT.TMath.Sqrt(
            excl_group_eff *
            (1.0 - excl_group_eff) / denom_evts) * args.input_rate
        rates_errors[f'{name}Exclusive'] = excl_group_rate_err

    # Sanitise the results so they align by decimal point
    pretty_rates = utils.dot_aligned(rates)
    pretty_rates_errors = utils.dot_aligned(rates_errors)
    for line in lines:
        print(
            "Line:\t {0:<{1}}\t Incl: {2} +/- {3} kHz,\tExcl: {4} +/- {5} kHz".
            format(line, length, pretty_rates[line][:7],
                   pretty_rates_errors[line][:6],
                   pretty_rates[line + 'Exclusive'][:7],
                   pretty_rates_errors[line + 'Exclusive'][:6]))
    print('-' * 110)
    if groups:
        print('Rates grouped by common physics: ')
        for name, lines_in_group in groups.items():
            print('INFO:\t %s group is formed of %s.' %
                  (name, ', '.join(lines_in_group)))
        print('-' * 110)
        for name in groups.keys():
            print(
                "Group:\t {0:<{1}}\t Incl: {2} +/- {3} kHz, Excl: {4} +/- {5} kHz"
                .format(name + ' lines', length, pretty_rates[name][:7],
                        pretty_rates_errors[name][:6],
                        pretty_rates[name + 'Exclusive'][:7],
                        pretty_rates_errors[name + 'Exclusive'][:6]))
        print('-' * 110)

    # We also want the total rate - this should be #events that fire any trigger/#events run over
    for level, lines_this_level in zip(("Hlt1", "Hlt2"),
                                       (hlt1_lines, hlt2_lines)):
        if lines_this_level == []: continue
        total_eff = float(
            tree.GetEntries(get_numer(
                " || ".join(lines_this_level)))) / denom_evts
        total_rate = total_eff * args.input_rate
        rates[f'{level}Total'] = total_rate
        total_rate_err = ROOT.TMath.Sqrt(
            total_eff * (1.0 - total_eff) / denom_evts) * args.input_rate
        rates_errors[f'{level}Total'] = total_rate_err
        print("{0} Total: {1:<{2}} Rate:\t {3:.0f} +/- {4:.1f} kHz".format(
            level, '', length, rates[f'{level}Total'],
            rates_errors[f'{level}Total']))
        if args.plot_total_rate:
            lines.append(f'{level}Total')

    if args.plot_group_rates:
        if not groups:
            print(
                "WARNING:\t Don\'t have any groups of lines defined. Not plotting group rates..."
            )
        else:
            for name in groups.keys():
                lines.append(name)

    print('-' * 110)
    print('Finished printing HLT rates!')

    def make_bar_chart(lines):
        level = get_level(lines[0])
        if not (all([utils.isHlt1(line) for line in lines])
                or all([utils.isHlt2(line) for line in lines])):
            raise RuntimeError(
                f"FATAL:\t You must specify lines from the same level to make a sensible bar chart. Lines specified: {', '.join(lines)}"
            )
        c = ROOT.TCanvas()
        c.SetGridy()
        c.SetRightMargin(0.15)
        ROOT.gStyle.SetHistMinimumZero()

        rate_hist = ROOT.TH1F(f"{level}_rates", "", len(lines), 0, len(lines))
        for i, line in enumerate(lines):
            assert get_level(line) == level, ""
            rate_hist.SetBinContent(i + 1, rates[line])
            rate_hist.SetBinError(i + 1, rates_errors[line])
            rate_hist.GetXaxis().SetBinLabel(i + 1, utils.sanitise_line(line))

        rate_hist.SetFillColor(ROOT.kBlue - 10)
        rate_hist.SetBarWidth(0.8)
        rate_hist.SetBarOffset(0.1)
        rate_hist.GetXaxis().SetTitle("")
        rate_hist.GetYaxis().SetTitle(f"{level} Rate / kHz")
        rate_hist.SetMaximum(
            max([rates[line] + rates_errors[line] for line in lines]) * 1.1)
        rate_hist.Draw('HIST BAR ')
        ROOT.gStyle.SetErrorX(0)
        rate_hist.SetLineColor(ROOT.kBlack)
        rate_hist.Draw('SAME E ')

        filter_path = f"wrt{filter_nickname.replace(' || ', 'OR')}"
        c.Print(
            f'{args.plot_prefix}Rates_{f"{filter_path}_" if filter_cut else ""}{level}.{args.plot_format}'
        )

    if args.plot_rates:
        # Make a bar chart of the rates.

        ROOT.TH1.SetDefaultSumw2()
        print('-' * 110)
        print('INFO:\t Making bar chart(s) of the rates...')
        print('-' * 110)

        for lines in (hlt1_lines, hlt2_lines):
            if lines:
                make_bar_chart(lines)

        print('-' * 110)
        print('INFO\t Finished making plots. Goodbye.')
        print('-' * 110)

    if args.json:
        with open(args.json, 'w') as f:
            json.dump({
                'rates': rates,
                'rates_errors': rates_errors
            },
                      f,
                      indent=4)


if __name__ == "__main__":
    main()
