###############################################################################
# (c) Copyright 2019-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from HltEfficiencyChecker.config import run_chained_hlt_with_tuples
from RecoConf.reconstruction_objects import reconstruction
from Hlt2Conf.lines.topological_b import all_lines
from RecoConf.hlt1_tracking import make_SPmixed_raw_banks, make_RetinaCluster_raw_event, make_VeloClusterTrackingSIMD, make_RetinaClusters, get_global_measurement_provider, make_velo_full_clusters
from RecoConf.hlt2_tracking import get_UpgradeGhostId_tool
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction, make_fastest_reconstruction
from RecoConf.calorimeter_reconstruction import make_digits
from PyConf.Algorithms import VeloRetinaClusterTrackingSIMD, VPRetinaFullClustering


def make_lines():
    return [builder() for builder in all_lines.values()]


options.lines_maker = make_lines
options.set_input_and_conds_from_testfiledb(
    'upgrade_DC19_01_MinBiasMD_retinacluster')
options.evt_max = 50
options.ntuple_file = "hlt1_and_hlt2_rate_ntuple_retinacluster.root"

make_RetinaCluster_raw_event.global_bind(make_raw=make_SPmixed_raw_banks)
make_VeloClusterTrackingSIMD.global_bind(
    make_raw=make_RetinaCluster_raw_event,
    algorithm=VeloRetinaClusterTrackingSIMD)
make_RetinaClusters.global_bind(make_raw=make_RetinaCluster_raw_event)
get_UpgradeGhostId_tool.global_bind(velo_hits=make_RetinaClusters)
get_global_measurement_provider.global_bind(velo_hits=make_RetinaClusters)
make_velo_full_clusters.global_bind(
    make_raw=make_SPmixed_raw_banks, make_full_cluster=VPRetinaFullClustering)
make_digits.global_bind(calo_raw_bank=False)

from RecoConf.hlt1_muonid import make_muon_hits
make_muon_hits.global_bind(geometry_version=2)

# TODO stateProvider_with_simplified_geom must go away from option files
from RecoConf.global_tools import stateProvider_with_simplified_geom
with reconstruction.bind(from_file=False), hlt2_reconstruction.bind(
        make_reconstruction=make_fastest_reconstruction):
    run_chained_hlt_with_tuples(
        options, public_tools=[stateProvider_with_simplified_geom()])
