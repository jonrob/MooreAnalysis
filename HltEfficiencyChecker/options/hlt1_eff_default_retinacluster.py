###############################################################################
# (c) Copyright 2019-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from HltEfficiencyChecker.config import run_allen_in_moore_with_tuples
from RecoConf.hlt1_allen import allen_gaudi_config as hlt1_configuration

from RecoConf.hlt1_tracking import make_SPmixed_raw_banks, make_RetinaCluster_raw_bank, make_VeloClusterTrackingSIMD, make_velo_full_clusters
from PyConf.Algorithms import VeloRetinaClusterTrackingSIMD, VPRetinaFullClustering

from Configurables import HiveDataBrokerSvc

HiveDataBrokerSvc().OutputLevel = 2

decay = ("[${B_s0}B_s0 => "
         "( phi(1020) => ${Kplus0}K+ ${Kminus0}K-) "
         "( phi(1020) => ${Kplus1}K+ ${Kminus1}K-) ]CC")

options.set_input_from_testfiledb(
    'Upgrade_BsPhiPhi_MD_FTv4_DIGI_retinacluster')
options.evt_max = 100
options.set_conds_from_testfiledb(
    'Upgrade_BsPhiPhi_MD_FTv4_DIGI_retinacluster')

options.ntuple_file = "eff_ntuple_default.root"

make_RetinaCluster_raw_bank.global_bind(make_raw=make_SPmixed_raw_banks)
make_VeloClusterTrackingSIMD.global_bind(
    make_raw=make_RetinaCluster_raw_bank,
    algorithm=VeloRetinaClusterTrackingSIMD)
make_velo_full_clusters.global_bind(
    make_raw=make_SPmixed_raw_banks, make_full_cluster=VPRetinaFullClustering)

with hlt1_configuration.bind(sequence="hlt1_pp_default"):
    run_allen_in_moore_with_tuples(options, decay)
