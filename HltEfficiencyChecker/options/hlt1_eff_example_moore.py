###############################################################################
# (c) Copyright 2019-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from HltEfficiencyChecker.config import run_moore_with_tuples

decay = ("[${B_s0}B_s0 => "
         "( J/psi(1S) => ${muplus}mu+ ${muminus}mu- ) "
         "( phi(1020) => ${Kplus}K+ ${Kminus}K- )]CC")

options.set_input_from_testfiledb('upgrade_DC19_01_Bs2JPsiPhi_MD')
options.evt_max = 100
options.set_conds_from_testfiledb('upgrade_DC19_01_Bs2JPsiPhi_MD')

from RecoConf.calorimeter_reconstruction import make_digits
make_digits.global_bind(calo_raw_bank=False)

from RecoConf.hlt1_muonid import make_muon_hits
make_muon_hits.global_bind(geometry_version=2)

options.ntuple_file = "hlt1_moore_eff_ntuple.root"

run_moore_with_tuples(options, True, decay)
