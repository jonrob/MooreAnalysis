###############################################################################
# (c) Copyright 2019-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from HltEfficiencyChecker.config import run_allen_in_moore_with_tuples
from RecoConf.hlt1_allen import allen_gaudi_config as hlt1_configuration

options.set_input_from_testfiledb('upgrade_DC19_01_MinBiasMD')
options.evt_max = 100
options.set_conds_from_testfiledb('upgrade_DC19_01_MinBiasMD')

options.ntuple_file = "rate_ntuple_default.root"

with hlt1_configuration.bind(sequence="hlt1_pp_veloSP"):
    run_allen_in_moore_with_tuples(options)
