###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Standard imports
from Moore import options
from HltEfficiencyChecker.config import run_moore_with_tuples
from RecoConf.reconstruction_objects import reconstruction

# Imports for current HLT2 config running in the pit
from RecoConf.hlt2_tracking import (
    make_PrKalmanFilter_noUT_tracks, make_PrKalmanFilter_Seed_tracks,
    make_PrKalmanFilter_Velo_tracks, make_TrackBestTrackCreator_tracks,
    get_UpgradeGhostId_tool_no_UT)
from RecoConf.hlt1_tracking import (
    make_VeloClusterTrackingSIMD, make_RetinaClusters,
    get_global_measurement_provider, make_reco_pvs, make_PatPV3DFuture_pvs)
from PyConf.Algorithms import VeloRetinaClusterTrackingSIMD
from RecoConf.hlt1_muonid import make_muon_hits
from RecoConf.calorimeter_reconstruction import make_digits

# HLT2 lines
from Hlt2Conf.lines.topological_b import all_lines


def make_hlt2_lines():
    return [builder() for builder in all_lines.values()]


options.lines_maker = make_hlt2_lines

file_prefix = "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2023-VeloClosed10mm-MagDown-Nu1.4-25ns/hlt1_filtered/30000000/hlt1_filtered_pp_tuned_june13"
options.input_files = [
    f"{file_prefix}_{i}.mdf" for i in [
        0, 2, 3, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 17, 19, 21, 22, 24, 25, 26,
        27, 28, 29, 31, 33, 34, 35, 37, 38, 39, 40, 41, 42, 43, 44, 46, 47, 48,
        50, 51, 52, 54, 56, 57, 58, 59, 60, 63, 64, 65, 66, 67, 68, 69, 72, 73,
        74, 76, 77, 79, 81, 82, 83, 84, 85, 87, 89, 90, 91, 92, 93, 95, 96, 97,
        99, 100, 102, 103, 104, 105, 107, 108, 109, 111, 113, 114, 115, 116,
        117, 118, 119, 122, 123, 124, 125, 127, 128, 130, 131, 133, 134, 135,
        137, 139, 140, 141, 144, 145, 146, 148, 150, 151, 152, 153, 154, 156,
        157, 158, 159, 160, 161, 162, 166, 167, 168, 170, 171, 172, 174, 175,
        176, 177, 180, 183, 184, 185, 186, 187, 190, 191, 192, 193, 194, 195,
        196, 197, 198, 199, 200, 201, 203, 205, 206, 207, 208, 209, 211, 212,
        213, 214, 215, 217, 218, 221, 223, 225, 226, 227, 229, 230, 231, 232,
        233, 234, 235, 237, 240, 241, 242, 244, 245, 246, 247, 248, 249, 250,
        252, 253, 256, 257, 258, 259, 260, 261, 263, 264, 265, 267, 269, 271,
        272, 273, 274, 275, 276, 278, 279, 280, 281, 283, 285, 286, 287, 289,
        290, 291, 292, 293, 294, 295, 296, 297, 298, 300, 301, 302, 303, 304,
        305, 306, 307, 309, 311, 312, 313, 314, 316, 317, 319, 320, 321, 322,
        324, 325, 326, 327, 329, 330, 331, 332, 334, 335, 336, 337, 339, 340,
        341, 343, 344, 345, 346, 347, 348, 349, 353, 354, 355, 356, 358, 360,
        361, 362, 364, 366, 367, 369, 370, 371, 372, 373, 375, 376, 377, 378,
        380, 381, 382, 384, 385, 387, 389, 390, 391, 394, 395, 396, 397, 398,
        400, 402, 404, 405, 406, 407, 408, 409, 410, 412, 414, 415, 416, 417,
        418, 419, 420, 421, 422, 423, 425, 426, 427, 428, 429, 430, 432, 433,
        435, 436, 438, 440, 441, 442, 445, 446, 447, 449, 450, 451, 453, 454,
        455, 456, 458, 461, 462, 463, 464, 467, 468, 469, 470, 473, 474, 475,
        477, 479, 480, 481, 483, 484, 485, 486, 487, 489, 490, 491, 492, 493,
        495, 496, 497, 498, 499, 500, 501, 502, 504, 506, 508, 509, 511, 513,
        514, 515, 516, 518, 519, 520, 522, 524, 525, 526, 527, 528, 530, 531,
        532, 534, 535, 536, 538, 539, 540, 542, 544, 545, 546, 548, 549, 550,
        551, 552, 555, 556, 557, 558, 559, 561, 564, 565, 566, 567, 568, 569,
        570
    ]
]  # Expected HLT1 output rate of this sample is ~0.95 MHz.
options.input_type = 'MDF'
options.dddb_tag = "dddb-20230313"
options.conddb_tag = "sim-20230322-vc-md100"

options.evt_max = 3000
options.simulation = True
options.data_type = 'Upgrade'
options.ntuple_file = "hlt2_rate_hlt1filtered.root"

from RecoConf.global_tools import stateProvider_with_simplified_geom
with reconstruction.bind(from_file=False),\
    make_TrackBestTrackCreator_tracks.bind(max_chi2ndof=4.2),\
    make_PrKalmanFilter_Velo_tracks.bind(max_chi2ndof=4.2),\
    make_PrKalmanFilter_noUT_tracks.bind(max_chi2ndof=4.2),\
    make_PrKalmanFilter_Seed_tracks.bind(max_chi2ndof=4.2),\
    make_VeloClusterTrackingSIMD.bind(algorithm=VeloRetinaClusterTrackingSIMD, SkipForward=4),\
    get_UpgradeGhostId_tool_no_UT.bind(velo_hits=make_RetinaClusters),\
    make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs),\
    get_global_measurement_provider.bind(velo_hits=make_RetinaClusters),\
    make_digits.bind(calo_raw_bank=True):
    run_moore_with_tuples(
        options, False, public_tools=[stateProvider_with_simplified_geom()])
