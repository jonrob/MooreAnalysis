#!/bin/bash
###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
set -euxo pipefail

default_dir=checker-hlt1-examples-$(date '+%Y%m%d-%H%M%S')
dir=${1:-$default_dir}
sequence=${2:-default}

mkdir -p $dir
cd $dir

# Efficiency

gaudirun.py \
    "${HLTEFFICIENCYCHECKERROOT}/options/hlt1_eff_${sequence}_retinacluster.py" \
    -o "eff_job_${sequence}.opts.py" --all-opts \
    2>&1 | tee "eff_job_${sequence}".log

$HLTEFFICIENCYCHECKERROOT/scripts/hlt_line_efficiencies.py \
    --reconstructible-children=Kplus0,Kminus0,Kplus1,Kminus1 \
    --legend-header="B^{0}_{s} #rightarrow #phi#phi" \
    --make-plots \
    "eff_ntuple_${sequence}.root" \
    2>&1 | tee "eff_results_${sequence}".log


# Rates

gaudirun.py \
    "$HLTEFFICIENCYCHECKERROOT/options/hlt1_rate_${sequence}_retinacluster".py \
    -o "rate_job_${sequence}.opts.py" --all-opts \
    2>&1 | tee "rate_job_${sequence}.log"

$HLTEFFICIENCYCHECKERROOT/scripts/hlt_calculate_rates.py \
    --plot-rates --plot-total-rate --plot-group-rates \
    --json "Hlt1_rates_${sequence}.json" \
    "rate_ntuple_${sequence}.root" \
    2>&1 | tee "rate_results_${sequence}.log"
