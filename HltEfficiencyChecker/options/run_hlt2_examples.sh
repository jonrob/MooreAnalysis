#!/bin/bash
###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
set -euxo pipefail

default_dir=checker-hlt2-examples-$(date '+%Y%m%d-%H%M%S')
dir=${1:-$default_dir}
mkdir -p $dir
cd $dir

# Efficiency

gaudirun.py \
    $HLTEFFICIENCYCHECKERROOT/options/hlt2_lines_example.py \
    $HLTEFFICIENCYCHECKERROOT/options/hlt2_eff_example.py \
    -o eff_job.opts.py --all-opts \
    2>&1 | tee eff_job.log

$HLTEFFICIENCYCHECKERROOT/scripts/hlt_line_efficiencies.py \
    --reconstructible-children=Kp,Km,mup,mum \
    --legend-header="B_{s} #rightarrow J/#psi #phi" \
    --make-plots \
    hlt2_eff_ntuple.root \
    2>&1 | tee eff_results.log

# Rates

gaudirun.py \
    $HLTEFFICIENCYCHECKERROOT/options/hlt2_lines_example.py \
    $HLTEFFICIENCYCHECKERROOT/options/hlt2_rate_example.py \
    -o rate_job.opts.py --all-opts \
    2>&1 | tee rate_job.log

$HLTEFFICIENCYCHECKERROOT/scripts/hlt_calculate_rates.py \
    --plot-rates --plot-total-rate --plot-group-rates \
    --json Hlt2_rates.json \
    hlt2_rate_ntuple.root \
    2>&1 | tee rate_results.log
