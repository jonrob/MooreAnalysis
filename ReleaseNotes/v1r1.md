2021-07-08 MooreAnalysis v1r1
===

This version uses
Analysis [v34r0](../../../../Analysis/-/tags/v34r0),
Moore [v52r2](../../../../Moore/-/tags/v52r2),
Allen [v1r4](../../../../Allen/-/tags/v1r4),
Phys [v33r0](../../../../Phys/-/tags/v33r0),
Rec [v33r0](../../../../Rec/-/tags/v33r0),
Lbcom [v33r0](../../../../Lbcom/-/tags/v33r0),
LHCb [v53r0](../../../../LHCb/-/tags/v53r0),
Gaudi [v36r0](../../../../Gaudi/-/tags/v36r0) and
LCG [100](http://lcginfo.cern.ch/release/100/) with ROOT 6.24.00.

This version is released on `master` branch.
Built relative to MooreAnalysis [v1r0](/../../tags/v1r0), with the following changes:

### New features ~"new feature"

- Add MCTupleToolTOS to HltEfficiencyChecker control flow, !33 (@rjhunter)

### Enhancements ~enhancement

- ~Persistency | Adapt to changes in Moore persistence, !32 (@sesen)
- Add option in the HltEfficiencyChecker yaml config to turn on DEBUG mode, !38 (@rjhunter)
- Initial streaming functionality, !36 (@nskidmor)
- Add --ignore-broken-inputs option to keep running HltEfficiencyChecker if encounter a dodgy file, !25 (@rjhunter)

### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Configuration | Adapt PyConf API to snake_case convention, !28 (@erodrigu)
- ~Functors | Adapt to ThOr functor and algorithm naming changes, !39 (@apearce)
- ~Functors | Add tests for ThOr-based HLT2 lines, !37 (@apearce)
- ~Persistency | Add suite of tests for HLT2 analysis, !30 (@apearce)
- ~Build | Add explicit dependencies to data packages (follow LHCb!3127), !40 (@rmatev)
- Pick up barrier algs from moore_control_flow, !29 (@rjhunter)
- Fix tests for Moore!664, !27 (@raaij) [#15]
- Increase timeouts for tests that have timed out in the last week, !24 (@rjhunter) [#14]
- Explicitly cast python map to list for python 2/3 compatibility in HltEfficiencyChecker wizard, !22 (@rjhunter)
