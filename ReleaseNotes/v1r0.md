2020-09-18 MooreAnalysis v1r0
===

This version uses
Moore [v51r1](../../../../Moore/-/tags/v51r1),
Analysis [v32r0](../../../../Analysis/-/tags/v32r0),
Allen [v1r0](../../../../Allen/-/tags/v1r0),
Phys [v31r1](../../../../Phys/-/tags/v31r1),
Rec [v31r1](../../../../Rec/-/tags/v31r1),
Lbcom [v31r1](../../../../Lbcom/-/tags/v31r1),
LHCb [v51r1](../../../../LHCb/-/tags/v51r1),
Gaudi [v33r2](../../../../Gaudi/-/tags/v33r2) and
LCG [97a](http://lcginfo.cern.ch/release/97a/) with ROOT 6.20.06.

This version is released on `master` branch.
This is the first version of MooreAnalysis, with the following features and changes:

- Avoid explicit MC unpacker scheduling, !20 (@apearce)
- Update README to reflect recent changes, !18 (@rjhunter)
- Add option to run the full HLT2 reconstruction in HltEfficiencyChecker jobs, !17 (@rjhunter)
- Follow changes in Moore!541, !15 (@sstahl)
- Refactor analysis scripts, !14 (@rmatev) [#12]
- Refactor HltEfficiencyChecker and support HLT2, !12 (@rmatev) [#1,#11,#9]
- Plotting tweaks, !9 (@rjhunter)
- Add the ability to calculate rates in HltEfficiencyChecker, !8 (@rjhunter)
- Add GEC to run in every event, !7 (@rjhunter)
- Update readme with instructions of how to build and run, !6 (@rjhunter)
- Fix some fragile and decay-specific hacks in the HltEfficiencyChecker tool, !5 (@rjhunter) [#3]
- HLT efficiency checker for Allen, !4 (@dovombru)
- Fix configuration of dependencies, !3 (@clemenci)
- Add HltEfficiencyChecker tool to mooreanalysis, !1 (@rjhunter)
