# MooreAnalysis

MooreAnalysis is a simple project that brings together the runtime
environment of Moore and Analysis. It hosts code for configuration
of jobs used to measure physics performance and contains scripts
for the analysis of the output of such jobs.
The actual configuration of the benchmarked application is in Moore
while the components used for writing out the numbers (tuple tools)
are in Analysis.

## How to build MooreAnalysis

First of all, you need to get an LHCb software stack, as per the instructions
[here](https://gitlab.cern.ch/rmatev/lb-stack-setup/). The necessary project
repositories will be cloned automatically.

With the configuration now set up, return to the terminal build the project as per the
instructions in `lb-stack-setup` (link above). When you hit `make MooreAnalysis`, all
the necessary projects will be cloned and compiled, although compilation will probably
take awhile. You should then be able to start
using the tools!

## Using HltEfficiencyChecker

Full up-to-date documentation, including how to build and run `HltEfficiencyChecker` with a series
of examples, can be found in the Moore online documentation [here](https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/hltefficiencychecker.html).