/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "DecayTreeTupleBase/DecayTreeTupleBase.h"
#include "DecayTreeTupleBase/OnePart.h"

#include "Kernel/Escape.h"

#include "DetDesc/IDetectorElement.h"

#include "boost/lexical_cast.hpp"

#include <algorithm>
#include <functional>

/**
 * \brief This is the new version of the so-called DecayChainNTuple
 *
 * \section basics (Basic usage:)
 *
 * DecayTreeTuple has a decay descriptor (given by the Decay
 * property). One line of the Tuple if filled for each reconstructed
 * candidate.
 *
 * For examples on how to use it, refer to Tutorial 8.
 * See https://twiki.cern.ch/twiki/bin/view/LHCb/DaVinciTutorial
 *
 * \section principle How it works:
 *
 * DecayTreeTuple basically owns a set of tools which all implement
 * either the IEventTupleTool or the IParticleTupleTool
 * interfaces. For every reconstructed candidates, it successively
 * call all the tools:
 *
 * - Once for each matched particles for the IParticleTupleTool instances
 *
 * - Once for the candidate for the IEventTupleTool instances
 *
 * \subsection branches Fine tuning of the branches:
 *
 * The \b Branches property, is a map<string,string>. Each of its
 * entry associates a name with decay descriptor matching a specific
 * part of the main decay.
 *
 * Once an entry exists, specific tools for this entry can be
 * added. They will only act on the particles matched by the
 * associated decay AND the main decay. Specific tools have their own
 * properties, whether or not the tool already exists in the generic
 * tool list.
 *
 * \note Notice the way the decay head is matched as it is a bit unusual.
 *
 * \subsection names Naming the tuple column:
 *
 * The following rules applies to name the tuple columns:
 *<ol>
 *
 * <li>By default, the name of the particles from the first matched
 * candidate are taken to prefix the column names (after having
 * sanitized some ugly chars).
 *
 * <li>If a specific decay is given for some particles, the first
 * argument of the \b Branches property is taken as prefix. This is not
 * true anymore if \b UseToolNameForBranchName is set to false.
 *
 * <li>For the nostalgics of DecayChainNTuple, the \b UseLabXSyntax
 * allows to prefix the branches with the good old \em labX style. Yet it
 * will be prefixed, not post fixed.
 *
 * </ol>
 *
 * \sa TupleToolDecay IEventTupleTool IParticleTupleTool
 *
 * \author Jeremie Borel with the help of Patrick and lhcb-davinci@cern.ch
 * \date 2007-11-01
 */
struct DecayTreeTuple : DecayTreeTupleBase {

  DecayTreeTuple( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode initialize() override;
  StatusCode execute() override;
};

using namespace LHCb;
using namespace Gaudi;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
DecayTreeTuple::DecayTreeTuple( const std::string& name, ISvcLocator* pSvcLocator )
    : DecayTreeTupleBase( name, pSvcLocator ) {
  // fill some default value
  m_toolList.push_back( "TupleToolKinematic" );
  m_toolList.push_back( "TupleToolPid" );
  m_toolList.push_back( "TupleToolGeometry" );
  m_toolList.push_back( "TupleToolEventInfo" );
  declareProperty( "ToolList", m_toolList );
  setProperty( "TupleName", "DecayTree" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode DecayTreeTuple::initialize() {
  StatusCode sc = DecayTreeTupleBase::initialize();
  if ( sc.isFailure() ) { return Error( "Error from base class", sc ); }
  sc = initializeDecays( false ) ? StatusCode::SUCCESS : StatusCode::FAILURE;
  if ( sc.isFailure() ) { return Error( "Error from initializeDecays(false)" ); }
  return sc;
}

//=============================================================================
// Execute
//=============================================================================
StatusCode DecayTreeTuple::execute() {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;
  ++m_eventCounter;

  LHCb::Particle::ConstVector heads;
  bool                        found = false;
  if ( useLoKiDecayFinders() ) {
    std::copy_if( i_particles().begin(), i_particles().end(), std::back_inserter( heads ), std::cref( decayTree() ) );
    found = !heads.empty();
  } else {
    const LHCb::Particle::ConstVector mothers( this->particles().begin(), this->particles().end() );
    if ( mothers.empty() ) {
      setFilterPassed( false );
      return StatusCode::SUCCESS;
    }
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << "I have " << mothers.size() << " particles to handle" << endmsg;
    StatusCode test = getDecayMatches( mothers, heads ) ? StatusCode::SUCCESS : StatusCode::FAILURE;
    found           = test.isSuccess();
  }
  if ( found ) {
    if ( msgLevel( MSG::VERBOSE ) )
      verbose() << "There is " << heads.size() << " top particles matching the decay." << endmsg;
  } else {
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << "No particle matching the decay." << endmsg;
    setFilterPassed( false );
    return StatusCode::SUCCESS;
  }

  // don't create the ntuple if there's nothing to fill!
  Tuple      tuple = nTuple( tupleName(), tupleName() );
  StatusCode test;
  if ( useLoKiDecayFinders() ) {
    test = fillTuple( tuple, heads, decayTree(), geometry() );
  } else {
    test = fillTuple( tuple, heads, dkFinder(), geometry() );
  }

  if ( test.isSuccess() ) {
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << "NTuple sucessfully filled" << endmsg;
  }

  setFilterPassed( test.isSuccess() );

  // Mandatory. Set to true if event is accepted.
  return test;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( DecayTreeTuple )
