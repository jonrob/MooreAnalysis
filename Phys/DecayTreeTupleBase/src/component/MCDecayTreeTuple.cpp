/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "DecayTreeTupleBase/DecayTreeTupleBase.h"
#include "DecayTreeTupleBase/OnePart.h"

#include "Kernel/Escape.h"

#include "DetDesc/IDetectorElement.h"

#include "boost/lexical_cast.hpp"

#include <algorithm>
#include <functional>

/**
 * \brief This is the new version of the so-called DecayChainNTuple
 *
 * \section basics (Basic usage:)
 *
 * MCDecayTreeTuple has a decay descriptor (given by the Decay
 * property). One line of the Tuple if filled for each reconstructed
 * candidate. This algorithm is NOT backward compatible with
 * DecayChainNTuple. Yet the base syntax is quite close.
 *
 * Here is a minimalist but working example:
 * \verbatim
 from Configurables import MCDecayTreeTuple, GaudiSequencer
 MyMCDecayTreeTuple = MCDecayTreeTuple("MyMCDecayTreeTuple")
 GaudiSequencer("MySeq").Members =+ [ MyMCDecayTreeTuple]
 # Decay descriptor: thick arrow to take into account additional photons from PHOTOS!
 MyMCDecayTreeTuple.Decay = "[ ( [B0]nos | [B~0]os ) => ^K+ ^pi-]CC"
 MyMCDecayTreeTuple.TupleName = "MyTuple"\endverbatim
 *
 * \note Any particle to be stored in the Tuple has to be flagged with
 * '^' (an exception here is the decay head which cannot be flagged as
 * DecayFinder will refuse it. Top level particle are therefore always
 * stored).

 * \sa DecayTreeTuple IEventTupleTool IMCParticleTupleTool
 *
 * \author Jeremie Borel with the help of Patrick and lhcb-davinci@cern.ch
 * \date 2007-11-01
 */
class MCDecayTreeTuple : public DecayTreeTupleBase {

public:
  MCDecayTreeTuple( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode initialize() override;
  StatusCode execute() override;

private:
  mutable Gaudi::Accumulators::Counter<> m_eventCounter{this, "Event"};
};

using namespace LHCb;
using namespace Gaudi;

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MCDecayTreeTuple )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MCDecayTreeTuple::MCDecayTreeTuple( const std::string& name, ISvcLocator* pSvcLocator )
    : DecayTreeTupleBase( name, pSvcLocator ) {
  // fill some default value
  m_toolList.push_back( "MCTupleToolKinematic" );
  m_toolList.push_back( "TupleToolEventInfo" );
  declareProperty( "ToolList", m_toolList );
  setProperty( "TupleName", "MCDecayTree" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode MCDecayTreeTuple::initialize() {
  const StatusCode sc = DecayTreeTupleBase::initialize();
  if ( sc.isFailure() ) return sc;
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;

  info() << "Tools to be used : ";
  for ( std::vector<std::string>::const_iterator s = m_toolList.begin(); s != m_toolList.end(); ++s ) {
    tool<IAlgTool>( *s, this );
    info() << *s << ", ";
  }
  info() << endmsg;

  initializeStufferTools( m_mcTools );

  if ( sc && initializeDecays( true ) ) {
    return StatusCode::SUCCESS;
  } else {
    return StatusCode::FAILURE;
  }
}

//=============================================================================
// Execute
//=============================================================================
StatusCode MCDecayTreeTuple::execute() {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;
  ++m_eventCounter;

  LHCb::MCParticle::ConstVector heads;
  bool                          found = false;
  if ( useLoKiDecayFinders() ) {
    const LHCb::MCParticles* mcParts = get<LHCb::MCParticles>( LHCb::MCParticleLocation::Default );
    std::copy_if( mcParts->begin(), mcParts->end(), std::back_inserter( heads ), std::cref( mcDecayTree() ) );
    found = !heads.empty();
  } else {
    LHCb::MCParticle::ConstVector mothers;
    const LHCb::MCParticle*       head = 0;
    while ( mcdkFinder()->findDecay( head ) ) { mothers.push_back( head ); }
    if ( mothers.empty() ) {
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << "No mothers of decay " << headDecay() << " found" << endmsg;
      setFilterPassed( false );
      return StatusCode::SUCCESS;
    }
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << "I have " << mothers.size() << " particles to handle" << endmsg;
    found = getDecayMatches( mothers, heads );
  }
  if ( found ) {
    if ( msgLevel( MSG::VERBOSE ) )
      verbose() << "There is " << heads.size() << " top particles matching the decay." << endmsg;
  } else {
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << "No particle matching the decay." << endmsg;
    setFilterPassed( false );
    return StatusCode::SUCCESS;
  }

  // don't create the ntuple if there's nothing to fill!
  Tuple      tuple = nTuple( tupleName(), tupleName() );
  StatusCode test;
  if ( useLoKiDecayFinders() ) {
    test = fillTuple( tuple, heads, mcDecayTree(), geometry() );
  } else {
    test = fillTuple( tuple, heads, mcdkFinder(), geometry() );
  }

  if ( test.isSuccess() ) {
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << "NTuple sucessfully filled" << endmsg;
  }

  setFilterPassed( test.isSuccess() );
  // Mandatory. Set to true if event is accepted.
  return test;
}
