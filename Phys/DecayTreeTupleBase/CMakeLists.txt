###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/DecayTreeTupleBase
-----------------------
#]=======================================================================]

gaudi_add_library(DecayTreeTupleBaseLib
    SOURCES
        src/lib/DecayTreeTupleBase.cpp
        src/lib/OnePart.cpp
    LINK
        PUBLIC
            Boost::headers
            Gaudi::GaudiAlgLib
            Gaudi::GaudiKernel
            LHCb::CaloFutureUtils
            LHCb::LHCbKernel
            LHCb::LoKiMCLib
            LHCb::MCEvent
            LHCb::MCInterfaces
            Rec::DaVinciInterfacesLib
            Rec::DaVinciKernelLib
            Rec::DaVinciMCKernelLib
            Rec::LoKiPhysLib
        PRIVATE
            LHCb::LoKiCoreLib
)

gaudi_add_module(DecayTreeTupleBase
    SOURCES
        src/component/DecayTreeTuple.cpp
        src/component/EventTuple.cpp
        src/component/MCDecayTreeTuple.cpp
        src/component/TupleToolDecay.cpp
    LINK
        Boost::headers
        DecayTreeTupleBaseLib
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::LHCbKernel
        LHCb::LoKiCoreLib
        Rec::DaVinciKernelLib
)

gaudi_add_dictionary(DecayTreeTupleBaseDict
    HEADERFILES dict/dictionary.h
    SELECTION dict/selection.xml
    LINK DecayTreeTupleBaseLib
)

