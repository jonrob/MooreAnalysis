/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <DetDesc/IGeometryInfo.h>
#include <GaudiKernel/IAlgTool.h>
#include <string>

namespace Tuples {
  class Tuple;
}
namespace LHCb {
  class Particle;
}

/**
 * Fill some particle related variables into a tuple.
 *
 * \sa IEventTupleTool
 *
 * @author Jeremie Borel
 * @date November 2007
 */
struct GAUDI_API IParticleTupleTool : extend_interfaces<IAlgTool> {

  DeclareInterfaceID( IParticleTupleTool, 3, 0 );

  //! Fill the tuple.
  //! - \b top  : may be used to provide additional information about \b part, like the top particle of the decay.
  //! - \b part : the particle about which some info are filled.
  //! - \b head : prefix for the tuple column name.
  //! - \b tuple: the tuple to fill
  virtual StatusCode fill( LHCb::Particle const* top, LHCb::Particle const* part, std::string const& head,
                           Tuples::Tuple& tuple, IGeometryInfo const& geometry ) = 0;
};
