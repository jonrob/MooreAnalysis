/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AsctAlgorithm.h"
#include "Event/MCParticle.h"
#include "Event/Particle.h"
#include "Event/ProtoParticle.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "Kernel/Particle2MCLinker.h"
#include <string>

/** @class Particle2MCLinks Particle2MCLinks.h
 *
 *
 *  @author Philippe Charpentier
 *  @date   10/05/2002
 */
class Particle2MCLinks : public GaudiAlgorithm {
public:
  /// Standard constructor
  Particle2MCLinks( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  std::vector<std::string>                  m_chargedPPLocation = {LHCb::ProtoParticleLocation::Charged,
                                                  LHCb::ProtoParticleLocation::Upstream};
  std::vector<std::string>                  m_neutralPPLocation;
  Gaudi::Property<std::vector<std::string>> m_inputData{this, "InputData", {}}; ///< location of input data in the TES
};

using namespace LHCb;

//-----------------------------------------------------------------------------
// Implementation file for class : Particle2MCLinks
//
// 10/05/2002 : Philippe Charpentier
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory

DECLARE_COMPONENT( Particle2MCLinks )

#define _verbose                                                                                                       \
  if ( msgLevel( MSG::VERBOSE ) ) verbose()
#define _debug                                                                                                         \
  if ( msgLevel( MSG::DEBUG ) ) debug()
#define _info                                                                                                          \
  if ( msgLevel( MSG::INFO ) ) info()

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Particle2MCLinks::Particle2MCLinks( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator ) {
  declareProperty( "ChargedPPLocation", m_chargedPPLocation );
  declareProperty( "NeutralPPLocation", m_neutralPPLocation );
}

//=============================================================================
// Initialisation. Check parameters
//=============================================================================
StatusCode Particle2MCLinks::initialize() {

  _debug << "==> Initialise" << endmsg;

  StatusCode sc = GaudiAlgorithm::initialize();
  if ( !sc.isSuccess() ) return sc;

  // property setting for Neutral is based on context()
  // default setup in initialize() to please the linker design
  if ( m_neutralPPLocation.empty() ) {
    std::string out( context() );
    std::transform( context().begin(), context().end(), out.begin(), ::toupper );
    if ( out == "HLT" ) {
      m_neutralPPLocation.push_back( ProtoParticleLocation::HltNeutrals );
    } else {
      m_neutralPPLocation.push_back( ProtoParticleLocation::Neutrals );
    }
  }

  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode Particle2MCLinks::execute() {

  _debug << "==> Execute" << endmsg;

  // Create a helper class for each type of Protoparticles
  Object2MCLinker<LHCb::ProtoParticle> chargedLink( this, Particle2MCMethod::ChargedPP, m_chargedPPLocation );
  Object2MCLinker<LHCb::ProtoParticle> neutralLink( this, Particle2MCMethod::NeutralPP, m_neutralPPLocation );

  //////////////////////////////////
  // Loop on Particles containers //
  //////////////////////////////////

  for ( const auto& inp : m_inputData ) {
    // create a Linker table for this Particle container
    LHCb::LinksByKey* linkerTable = LHCb::LinksByKey::fetchFromOrCreateOnTES<LHCb::ProtoParticle, LHCb::MCParticle>(
        evtSvc(), inp + Particle2MCMethod::extension[Particle2MCMethod::Links],
        LHCb::LinksByKey::Order::decreasingWeight );
    if ( !linkerTable ) continue;

    // Get Particles
    Particles const* parts = getIfExists<Particles>( inp );
    if ( !parts ) {
      _verbose << "Could not find container " << inp << endmsg;
      continue;
    }
    int npp  = parts->size();
    int nrel = 0;
    int nass = 0;
    _debug << "    " << npp << " Particles retrieved from " << inp << endmsg;

    // loop on Parts and MCParts to match them
    for ( const Particle* part : *parts ) {
      _verbose << "Particle " << part->momentum() << endmsg << "    Particle " << objectName( part );

      auto* link = part->charge() ? &chargedLink : &neutralLink;
      // check if it is from a ProtoParticle
      const ProtoParticle* protoPart = part->proto();
      if ( !protoPart ) {
        verbose() << " is not from a ProtoParticle";
        continue;
      }
      if ( msgLevel( MSG::VERBOSE ) ) {
        std::string strCharged = part->charge() ? "Charged" : "Neutral";
        verbose() << " from " << strCharged << " ProtoParticle " << protoPart->key();
      }
      // Use the Linker
      auto mcParts = link->range( protoPart );
      if ( mcParts.empty() ) {
        _verbose << " is not associated to any MCPart" << endmsg;
        continue;
      }
      nass++;
      _verbose << " is associated to MCParts:" << endmsg;
      for ( const auto& [mcPart, weight] : mcParts ) {
        _verbose << "     " << mcPart.key() << " " << mcPart.particleID().pid() << " " << mcPart.momentum() << endmsg;
        linkerTable->link( part, &mcPart, weight );
        nrel++;
      }
    }
    _debug << "Out of " << npp << " Particles in " << inp << ", " << nass << " are associated, " << nrel
           << " relations found" << endmsg;
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
