/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
// Implementation file for class : ChargedPP2MC
//
// 10/07/2002 : Philippe Charpentier
//-----------------------------------------------------------------------------
#include "Event/MCParticle.h"
#include "Event/ProtoParticle.h"
#include "Event/Track.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "Kernel/PP2MCLocation.h"
#include "Kernel/Particle2MCLinker.h"
#include "Relations/RelationWeighted1D.h"

static const std::string& ChargedPP2MCAsctLocation = LHCb::ProtoParticle2MCLocation::Charged;
using namespace LHCb;

class ChargedPP2MC : public GaudiAlgorithm {
public:
  ChargedPP2MC( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode execute() override;

private:
  bool m_vetoempty;

private:
  Gaudi::Property<std::vector<std::string>> m_inputData{
      this,
      "InputData",
      {ProtoParticleLocation::Charged, ProtoParticleLocation::Upstream,
       ProtoParticleLocation::HltCharged}}; ///< location of input data in the TES
  Gaudi::Property<std::string> m_outputTable{this, "OutputTable",
                                             ChargedPP2MCAsctLocation};     ///< location of relations table
  std::vector<std::string>     m_trackLocations = {TrackLocation::Default}; ///< location for tracks
};

// Define data types
typedef RelationWeighted1D<ProtoParticle, MCParticle, double> Table;

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( ChargedPP2MC )

#define _verbose                                                                                                       \
  if ( msgLevel( MSG::VERBOSE ) ) verbose()
#define _debug                                                                                                         \
  if ( msgLevel( MSG::DEBUG ) ) debug()
#define _info                                                                                                          \
  if ( msgLevel( MSG::INFO ) ) info()

ChargedPP2MC::ChargedPP2MC( const std::string& name, ISvcLocator* pSvcLocator ) : GaudiAlgorithm( name, pSvcLocator ) {
  declareProperty( "TrackLocations", m_trackLocations );
  declareProperty( "VetoEmpty", m_vetoempty = false );
}

StatusCode ChargedPP2MC::execute() {

  _debug << "==> Execute" << endmsg;

  // flag to create a Relations table if needed,
  const bool createTable = !m_outputTable.empty();

  /// Create the linker object
  Object2MCLinker<LHCb::Track> track2MCLink{this, "", "", m_trackLocations};

  ///////////////////////////////////////
  // Loop on ProtoParticles containers //
  ///////////////////////////////////////
  for ( auto inp = m_inputData.begin(); m_inputData.end() != inp; inp++ ) {
    // Get ProtoParticles
    const ProtoParticles* protos = getIfExists<ProtoParticles>( *inp );
    // Check here to avoid the rest running on uDSTs where Protos are missing
    if ( !protos ) {
      _debug << "No ProtoParticles at '" << ( *inp ) << "'" << endmsg;
      continue;
    }
    // Though we do not want to create empty containers in Turbo processing
    if ( !protos && m_vetoempty ) continue;

    // postpone the check for pointer till linker and relations table
    // are cretsaed/locate/registered

    // Create a linker table
    const std::string linkContainer = *inp + Particle2MCMethod::extension[Particle2MCMethod::ChargedPP];
    auto*             linkerTable   = LHCb::LinksByKey::fetchFromOrCreateOnTES<LHCb::ProtoParticle, LHCb::MCParticle>(
        evtSvc(), linkContainer, LHCb::LinksByKey::Order::decreasingWeight );

    Table* table = nullptr;
    if ( createTable ) {
      // Register the relations table on the TES using Marco's convention
      std::string loc = *inp;
      if ( loc.compare( 0, 7, "/Event/" ) == 0 ) { loc.erase( 0, 7 ); }
      //
      if ( !exist<DataObject>( "Relations/" + loc ) ) // needed to deal with multi-input -> multi-output
      {
        // create new table
        table = new Table( protos ? protos->size() : 0 );
        put( table, "Relations/" + loc );
      }
    }

    if ( !table ) continue;

    // and only here check the input data
    if ( !protos ) continue;

    int npp = protos->size();
    _verbose << "    " << npp << " ProtoParticles retrieved from " << *inp << endmsg;

    // loop on Protos to match them from their tracks
    int nrel = 0;
    int nass = 0;
    for ( const auto* pIt : *protos ) {
      _verbose << "    ProtoParticle " << pIt->key();
      // Follow links from this protoParticle via tracks
      const Track* track = pIt->track();
      if ( track ) {
        _verbose << " from track " << track->key();
        auto mcParts = track2MCLink.range( track );
        if ( mcParts.empty() ) {
          _verbose << " not associated to an MCPart";
        } else {
          _verbose << " associated to MCParts";
          nass++;
          for ( const auto& [mcPart, weight] : mcParts ) {
            _verbose << " - " << mcPart.key();
            if ( table ) table->relate( pIt, &mcPart, weight ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
            linkerTable->link( pIt, &mcPart, weight );
            nrel++;
          }
        }
      } else {
        _verbose << " not originating from a track";
      }
      _verbose << endmsg;
      if ( table && table->registry() ) {
        counter( "#" + table->registry()->identifier() ) += table->relations().size();
      }
    }
    _debug << "Out of " << npp << " Charged ProtoParts in " << *inp << ", " << nass << " are associated, " << nrel
           << " relations found" << endmsg;
  }

  return StatusCode::SUCCESS;
}
