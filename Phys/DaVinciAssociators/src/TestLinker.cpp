/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
// Implementation file for class : TestLinker
//
// Example of how to access various info from DaVinci
//
// 26/04/2002 : Philippe Charpentier
//-----------------------------------------------------------------------------

#include "Event/MCParticle.h"
#include "Event/MCTrackInfo.h"
#include "Event/Particle.h"
#include "Event/ProtoParticle.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/IToolSvc.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "Kernel/Particle2MCLinker.h"
#include <cmath>

/** @class TestLinker TestLinker.h
 *
 *  Example of use of various DaVinci features
 *
 *  @author Philippe Charpentier
 *  @date   26/04/2002
 */

#define TR_EFFICIENCY

class TestLinker : public GaudiAlgorithm {

public:
  /// Standard constructor
  TestLinker( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override;  ///< Algorithm execution
  StatusCode finalize() override; ///< Algorithm finalization

  void prTable( const MSG::Level level, const std::string title, const std::vector<int>& table, const int width );
  int  trType( const LHCb::Track* tr );

private:
  int                                       m_matchLinks;
  int                                       m_matchChi2;
  int                                       m_matchFull;
  int                                       m_matchLinksNotChi2;
  int                                       m_matchLinksHighChi2;
  int                                       m_matchChi2NotLinks;
  int                                       m_matchDifferent;
  int                                       m_matchLinksDiffComp;
  int                                       m_matchMissedComp;
  int                                       m_matchComp;
  int                                       m_nbParts;
  int                                       m_mcPartRecons;
  std::vector<int>                          m_mcPart2Track;
  std::vector<int>                          m_mcPart2Proto;
  std::vector<int>                          m_mcPart2Part;
  std::vector<int>                          m_mcPart2PartChi2;
  std::vector<int>                          m_mcPart2PartLink;
  int                                       m_mcPartCount;
  int                                       m_skippedEvts;
  int                                       m_nbEvts;
  bool                                      m_setInputData;
  Gaudi::Property<std::vector<std::string>> m_inputData{this, "InputData", {}}; ///< location of input data in the TES
};

using namespace LHCb;

#define _verbose                                                                                                       \
  if ( msgLevel( MSG::VERBOSE ) ) verbose()
#define _debug                                                                                                         \
  if ( msgLevel( MSG::DEBUG ) ) debug()
#define _info                                                                                                          \
  if ( msgLevel( MSG::INFO ) ) info()

// Declaration of the Algorithm Factory

DECLARE_COMPONENT( TestLinker )

//=============================================================================
// Standard creator, initializes variables
//=============================================================================
TestLinker::TestLinker( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator )
    , m_matchLinks( 0 )
    , m_matchChi2( 0 )
    , m_matchFull( 0 )
    , m_matchLinksNotChi2( 0 )
    , m_matchLinksHighChi2( 0 )
    , m_matchChi2NotLinks( 0 )
    , m_matchDifferent( 0 )
    , m_matchLinksDiffComp( 0 )
    , m_matchMissedComp( 0 )
    , m_matchComp( 0 )
    , m_nbParts( 0 )
    , m_mcPartRecons( 0 )
    , m_mcPart2Track( 7, 0 )
    , m_mcPart2Proto( 6, 0 )
    , m_mcPart2Part( 6, 0 )
    , m_mcPart2PartChi2( 6, 0 )
    , m_mcPart2PartLink( 6, 0 )
    , m_mcPartCount( 0 )
    , m_skippedEvts( 0 )
    , m_nbEvts( 0 )
    , m_setInputData( true ) {
  declareProperty( "SetInputData", m_setInputData );
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode TestLinker::execute() {

  int matchLinks = 0, matchChi2 = 0, matchFull = 0, nbParts = 0;
  int matchLinksNotChi2 = 0, matchLinksHighChi2 = 0, matchChi2NotLinks = 0, matchDifferent = 0, matchLinksDiffComp = 0,
      matchMissedComp = 0, matchComp = 0;
  int skippedEvt = 0;

  std::vector<std::string> inp = m_setInputData ? m_inputData.value() : std::vector<std::string>();

  // Retrieve the link objects used by this algorithm
  Particle2MCLinker linkComp( this, Particle2MCMethod::Composite, inp );
  Particle2MCLinker linkChi2( this, Particle2MCMethod::Chi2, inp );
  Particle2MCLinker linkWithChi2( this, Particle2MCMethod::WithChi2, inp );
  Particle2MCLinker linkLinks{this, Particle2MCMethod::Links, inp};

  ProtoParticle2MCLinker linkChargedPP( this, Particle2MCMethod::ChargedPP,
                                        std::vector{ProtoParticleLocation::Charged} );
  ProtoParticle2MCLinker linkNeutralPP( this, Particle2MCMethod::NeutralPP,
                                        std::vector{ProtoParticleLocation::Neutrals} );

  for ( auto const& inp : m_inputData ) {
    // Retrieve Particles
    const auto* parts = getIfExists<Particles>( inp );
    if ( !parts ) {
      _debug << "    No Particles retrieved from " << inp << endmsg;
      continue;
    } else {
      // msg number of Candidates retrieved
      _verbose << "    Number of Particles retrieved from " << inp << "  = " << parts->size() << endmsg;
      if ( parts->size() == 0 ) skippedEvt = 1;
    }

    //========================================
    // Example of use of the Associator tool
    //========================================

    // Using the direct association
    for ( const Particle* part : *parts ) {
      ++nbParts;

      std::string strCharged = part->charge() ? "Charged" : "Neutral";
      _verbose << "+++ " << strCharged << " particle " << part->key() << " " << part->particleID().pid()
               << " , momentum, slopes " << part->p() << " " << part->slopes().X() << " " << part->slopes().Y()
               << endmsg;

      const auto mcPartLinks = linkLinks.range( part );
      if ( !mcPartLinks.empty() ) {
        // An association was obtained from links
        _verbose << "    Associated to " << linkLinks.associatedMCP( part ) << " MCParts: " << endmsg;
        if ( part->charge() ) matchLinks++;
        int nass = 0;
        for ( const auto& [mcPart, weight] : mcPartLinks ) {
          ++nass;
          const Gaudi::LorentzVector mc4Mom = mcPart.momentum();
          _verbose << "    MCPart " << mcPart.key() << " " << mcPart.particleID().pid() << " (weight " << weight
                   << ") from links : momentum, slopes " << mc4Mom.Vect().R() << " " << mc4Mom.Px() / mc4Mom.Pz() << " "
                   << mc4Mom.Py() / mc4Mom.Pz() << endmsg;
        }

        // Reset the link to the first one to test the "decreasing" feature
        auto const* mcPartLink = linkLinks.range( part ).try_front();
        auto const* mcPartComp = linkComp.range( part ).try_front();
        if ( part->charge() ) {
          if ( mcPartComp ) {
            matchComp++;
            if ( mcPartLink != mcPartComp ) {
              _verbose << "    MCPart from Composite != Links" << endmsg;
              if ( mcPartLink ) matchLinksDiffComp++;
            }
          } else {
            matchMissedComp++;
          }
        }
      } else {
        _verbose << "    No MCPart found from links" << endmsg;
      }
      auto const* mcPartLink = mcPartLinks.try_front();
      if ( 0 == part->charge() ) continue;
      auto              withChi2Range = linkWithChi2.range( part );
      const MCParticle* mcPartChi2    = nullptr;
      double            chi2          = 0;
      if ( !withChi2Range.empty() ) {
        auto const& [mcp, c2] = *withChi2Range.begin();
        mcPartChi2            = &mcp;
        chi2                  = c2;
      }
      auto chi2Range = linkChi2.range( part );
      if ( chi2Range.empty() ) {
        // No particles found above threshold with CHi2
        _verbose << "      MCPart not found from Chi2 below threshold";
        if ( mcPartLink && ( mcPartChi2 == mcPartLink ) ) {
          _verbose << " (Chi2 was " << chi2 << ")";
          matchLinksHighChi2++;
        }
        _verbose << endmsg;
        if ( mcPartLink ) matchLinksNotChi2++;
      } else {
        if ( mcPartChi2 == mcPartLink ) {
          _verbose << "      MCPart found from Chi2 as well (Chi2 = " << chi2 << ")" << endmsg;
          matchFull++;
        }
        if ( !mcPartLink ) matchChi2NotLinks++;
      }
      if ( mcPartChi2 && ( mcPartChi2 != mcPartLink ) ) {
        if ( mcPartLink ) {
          matchDifferent++;
          _verbose << "      MCPart found from Chi2 is different" << endmsg;
        }
        const Gaudi::LorentzVector mc4Mom = mcPartChi2->momentum();
        _verbose << "      MCPart from Chi2 " << part->particleID().pid() << " : momentum, slope " << mc4Mom.Vect().R()
                 << " " << mc4Mom.Px() / mc4Mom.Pz() << " " << mc4Mom.Py() / mc4Mom.Pz() << endmsg;
        _verbose << "       Chi2 was " << chi2 << endmsg;
        auto partLinks = linkLinks.range( mcPartChi2 );
        if ( !partLinks.empty() ) {
          _verbose << "       It is linked to Particle " << std::get<0>( partLinks.front() ).key() << endmsg;
        }
      }
      if ( !chi2Range.empty() ) { ++matchChi2; }
    }
  }

  if ( 0 == nbParts ) return StatusCode::SUCCESS;

  int width = (int)log10( (double)nbParts + 1 ) + 1;

  _debug << "========= On " << std::setw( width ) << nbParts << " Particles =========" << endmsg
         << "   | Matched with Links | " << std::setw( width ) << matchLinks << " | Missed with Chi2  | "
         << std::setw( width ) << matchLinksNotChi2 << " | Too large Chi2    | " << std::setw( width )
         << matchLinksHighChi2 << endmsg << "   | Matched with Comp. | " << std::setw( width ) << matchComp
         << " | Diff. from Links  | " << std::setw( width ) << matchLinksDiffComp << " | Missed with Comp. | "
         << std::setw( width ) << matchMissedComp << endmsg << "   | Matched with Chi2  | " << std::setw( width )
         << matchChi2 << " | Missed with Links | " << std::setw( width ) << matchChi2NotLinks << endmsg
         << "   | Matched with both  | " << std::setw( width ) << matchFull << " | Matched different | "
         << std::setw( width ) << matchDifferent << endmsg;

  m_matchLinks += matchLinks;
  m_matchChi2 += matchChi2;
  m_matchFull += matchFull;
  m_matchLinksNotChi2 += matchLinksNotChi2;
  m_matchLinksHighChi2 += matchLinksHighChi2;
  m_matchChi2NotLinks += matchChi2NotLinks;
  m_matchLinksDiffComp += matchLinksDiffComp;
  m_matchMissedComp += matchMissedComp;
  m_matchComp += matchComp;
  m_matchDifferent += matchDifferent;
  m_nbParts += nbParts;

#ifdef TR_EFFICIENCY
  // Now use reverse associators to check how frequently
  //     an MCParticle is associated

  // Retrieve MCParticles
  const auto* mcParts = get<MCParticles>( MCParticleLocation::Default );

  // Loop on MCParticles now
  std::vector<int> mcPart2Track( 7, 0 );
  std::vector<int> mcPart2Proto( 6, 0 );
  std::vector<int> mcPart2Part( 6, 0 );
  std::vector<int> mcPart2PartChi2( 6, 0 );
  std::vector<int> mcPart2PartLink( 6, 0 );

  int              mcPartCount  = 0;
  int              mcPartRecons = 0;
  std::vector<int> trackFound( 6, 0 );
  m_nbEvts++;

  if ( skippedEvt ) {
    m_skippedEvts++;
  } else {
    auto const* links = get<LHCb::LinksByKey>( LHCb::LinksByKey::linkerName( TrackLocation::Default ) );

    // Get the track info if present
    MCTrackInfo trackInfo = make_MCTrackInfo( evtSvc(), msgSvc() );
    for ( const MCParticle* mcPart : *mcParts ) {
      mcPartCount++;
      // Check whether it was the child of a B or a Ks
      //      if( isChild( "B0" ) ) {
      //        fromB0 = 1;
      //      }
      //      if( isChild( "K0s" ) ) {
      //        fromK0 = 1;
      //      }
      // Check if it was reconstructible
      if ( trackInfo.hasVeloAndT( mcPart ) ) {
        verbose() << "    MCParticle " << mcPart->key() << endmsg << "      Is reconstructable as Long track" << endmsg;
        ++mcPartRecons;
        bool countTr    = true;
        bool countProto = true;
        for ( const auto& [tr, _] : LinkedFrom<Track>{links}.weightedRange( mcPart ) ) {
          verbose() << "      Is associated to track " << tr.key();
          if ( !tr.checkFlag( Track::Flags::Clone ) ) {
            verbose() << " that is unique" << endmsg;
            // Look at the type of track
            int type = trType( &tr );
            if ( 0 == type ) continue;
            if ( countTr ) mcPart2Track[0]++;
            mcPart2Track[type]++;
            countTr          = false;
            trackFound[type] = 1;
            // Check the protoPart comes from that track
            ProtoParticle2MCLinker* protoLink = mcPart->particleID().threeCharge() ? &linkChargedPP : &linkNeutralPP;

            for ( const auto& [proto, _] : protoLink->range( mcPart ) ) {
              verbose() << "      and gave ProtoParticle " << proto.key() << endmsg;
              if ( proto.track() == &tr ) {
                if ( countProto ) mcPart2Proto[0]++;
                mcPart2Proto[type]++;
                countProto = false;
              }
            }
          } else {
            verbose() << " that is NOT unique" << endmsg;
          }
        }
        // Now look at association to Particles with all associators
        auto partLinks  = linkLinks.range( mcPart );
        int  partTrType = 0;
        if ( !partLinks.empty() ) {
          ++mcPart2PartLink[0];
          const auto& [part, _] = partLinks.front();
          if ( auto proto = part.proto(); proto ) {
            partTrType = trType( proto->track() );
            if ( partTrType ) mcPart2PartLink[partTrType]++;
          }
        }
        auto partChi2s = linkChi2.range( mcPart );
        if ( !partChi2s.empty() ) {
          ++mcPart2PartChi2[0];
          const auto& [part, _] = partChi2s.front();
          if ( auto proto = part.proto(); proto ) {
            partTrType = trType( proto->track() );
            if ( partTrType ) mcPart2PartChi2[partTrType]++;
          }
        }
        if ( !partLinks.empty() || !partChi2s.empty() ) {
          ++mcPart2Part[0];
          if ( partTrType ) mcPart2Part[partTrType]++;
        }
        if ( !trackFound[1] && !trackFound[2] && trackFound[3] && trackFound[4] ) { mcPart2Track[6]++; }
      }
    }
  }

  width = (int)log10( (double)mcPartCount + 1 ) + 1;
  _debug << "========= On " << std::setw( width ) << mcPartCount << " MCParticles =========" << endmsg
         << "   |                       |  Total  | Forward |  Match  |  Velo   |"
         << "  Seed   | Upstream|  Missed |" << endmsg;
  width = 7;
  _debug << "   | Reconstructible long  | " << std::setw( width ) << mcPartRecons << endmsg;

  prTable( MSG::DEBUG, "   | Linked to a track     | ", mcPart2Track, width );
  prTable( MSG::DEBUG, "   | Linked to a ProtoPart | ", mcPart2Proto, width );
  prTable( MSG::DEBUG, "   | Linked to a Part/Link | ", mcPart2PartLink, width );
  prTable( MSG::DEBUG, "   | Linked to a Part/Chi2 | ", mcPart2PartChi2, width );
  prTable( MSG::DEBUG, "   | Linked to a Particle  | ", mcPart2Part, width );

  // Collect full statistics
  m_mcPartRecons += mcPartRecons;
  m_mcPartCount += mcPartCount;
  unsigned int i = 0;
  for ( i = 0; i < m_mcPart2Track.size(); i++ ) { m_mcPart2Track[i] += mcPart2Track[i]; }
  for ( i = 0; i < m_mcPart2Proto.size(); i++ ) {
    m_mcPart2Proto[i] += mcPart2Proto[i];
    m_mcPart2Part[i] += mcPart2Part[i];
    m_mcPart2PartChi2[i] += mcPart2PartChi2[i];
    m_mcPart2PartLink[i] += mcPart2PartLink[i];
  }

#endif

  // End of execution for each event
  return StatusCode::SUCCESS;
}
void TestLinker::prTable( const MSG::Level level, const std::string title, const std::vector<int>& table,
                          const int width ) {
  if ( msgLevel( level ) ) {
    msgStream( level ) << title;
    for ( unsigned int i = 0; i < table.size(); i++ ) { msgStream( level ) << std::setw( width ) << table[i] << " | "; }
    msgStream( level ) << endmsg;
  }
}

int TestLinker::trType( const Track* tr ) {
  //
  // This mixes track types and how they have been found.
  // -> modify according to what this function is needed for
  //
  if ( !tr ) return 0;
  if ( tr->checkHistory( Track::History::PrForward ) ) return 1;
  if ( tr->checkHistory( Track::History::PrMatch ) ) return 2;
  if ( tr->checkHistory( Track::History::PrPixel ) ) return 3;
  if ( tr->checkHistory( Track::History::PrSeeding ) ) return 4;
  if ( tr->checkHistory( Track::History::PrDownstream ) ) return 5;
  return 0;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode TestLinker::finalize() {

  _debug << ">>> Finalize" << endmsg;
  int width = (int)log10( (double)m_nbParts + 1 ) + 1;

  _info << "======== Statistics for Particles to MCParticles association"
        << "========" << endmsg << "======== On " << std::setw( width ) << m_nbParts << " Particles ( " << m_nbEvts
        << " events) ========" << endmsg << "   | Matched with Links | " << std::setw( width ) << m_matchLinks
        << " | Missed with Chi2  | " << std::setw( width ) << m_matchLinksNotChi2 << " | Too large Chi2    | "
        << std::setw( width ) << m_matchLinksHighChi2 << endmsg << "   | Matched with Comp. | " << std::setw( width )
        << m_matchComp << " | Diff. from Links  | " << std::setw( width ) << m_matchLinksDiffComp
        << " | Missed with Comp. | " << std::setw( width ) << m_matchMissedComp << endmsg
        << "   | Matched with Chi2  | " << std::setw( width ) << m_matchChi2 << " | Missed with Links | "
        << std::setw( width ) << m_matchChi2NotLinks << endmsg << "   | Matched with both  | " << std::setw( width )
        << m_matchFull << " | Matched different | " << std::setw( width ) << m_matchDifferent << endmsg;

#ifdef TR_EFFICIENCY
  width = (int)log10( (double)m_mcPartCount + 1 ) + 1;
  _info << "======== Statistics on MCParticle associations ========" << endmsg << "======== On " << std::setw( width )
        << m_mcPartCount << " MCParticles ( " << m_nbEvts - m_skippedEvts << " events, " << m_skippedEvts
        << " skipped) ========" << endmsg << "   |                       |  Total  | Forward |  Match  |  Velo   |"
        << "  Seed   | Upstream|  Missed |" << endmsg;
  width = 7;
  _info << "   | Reconstructible long  | " << std::setw( width ) << m_mcPartRecons << endmsg;

  prTable( MSG::INFO, "   | Linked to a track     | ", m_mcPart2Track, width );
  prTable( MSG::INFO, "   | Linked to a ProtoPart | ", m_mcPart2Proto, width );
  prTable( MSG::INFO, "   | Linked to a Part/Link | ", m_mcPart2PartLink, width );
  prTable( MSG::INFO, "   | Linked to a Part/Chi2 | ", m_mcPart2PartChi2, width );
  prTable( MSG::INFO, "   | Linked to a Particle  | ", m_mcPart2Part, width );
#endif

  // End of finalization step
  return GaudiAlgorithm::finalize();
}
