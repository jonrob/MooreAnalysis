/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
// Implementation file for class : MCReconstructed
//
// 2004-03-08 : Christian Jacoby
//-----------------------------------------------------------------------------

/** @class MCReconstructed MCReconstructed.h
 *
 *
 *  @author Christian Jacoby
 *  @date   2004-03-08
 *
 *  The @c MCReconstructed tool determines if a @c MCParticle has been
 *  reconstructed by reverse association using @c ProtoParticle2MCLinker.
 *  That is, if the tested @c MCParticle has an associated @c ProtoParticle,
 *  then @c MCReconstructed considers it to have been reconstructed.
 *  The @c IMCReconstructed::RecCategory is determined by examining the charge
 *  of the @c MCParticle and the track type of the associated @c ProtoParticle.
 */

#include "Event/MCParticle.h"
#include "Event/ProtoParticle.h"
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/Particle2MCLinker.h"
#include "MCInterfaces/IMCReconstructed.h"

class MCReconstructed : public extends<GaudiTool, IMCReconstructed> {
public:
  /// Standard constructor
  using extends::extends;

  IMCReconstructed::RecCategory reconstructed( const LHCb::MCParticle* ) override;
};

// Declaration of the Tool Factory
DECLARE_COMPONENT( MCReconstructed )

//=============================================================================
//  Method to check if a particle is reconstructed
//  input:  MCParticle
//  output: 0 = not rec'ted (default), 1 = long, 2 = TTT, 3 = VTT, 4=NEUTRAL
//=============================================================================
IMCReconstructed::RecCategory MCReconstructed::reconstructed( const LHCb::MCParticle* iMCstable ) {

  if ( !iMCstable ) Exception( "NULL MCParticle" );
  IMCReconstructed::RecCategory rected = IMCReconstructed::NotReconstructed;

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "## Checking MCParticle: " << iMCstable->momentum() << " " << iMCstable->particleID().pid() << endmsg;
  }
  if ( iMCstable->particleID().threeCharge() != 0 ) ///< for charged
  {
    auto pCPPAsct = Object2FromMC<LHCb::ProtoParticle, GaudiTool>( this, Particle2MCMethod::ChargedPP,
                                                                   LHCb::ProtoParticleLocation::Charged );
    for ( const auto& [pp, w] : pCPPAsct.range( iMCstable ) ) {
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "    has an associated charged ProtoParticle " << &pp << " " << pp.track() << " " << pp.calo()
                << endmsg;
      const LHCb::Track* ptrack = pp.track();
      if ( msgLevel( MSG::DEBUG ) ) {
        if ( ptrack )
          debug() << "Track type is " << ptrack->type() << " clone? " << ptrack->checkFlag( LHCb::Track::Flags::Clone )
                  << endmsg;
        else
          debug() << "No track" << endmsg;
      }
      if ( ptrack && !ptrack->checkFlag( LHCb::Track::Flags::Clone ) ) {
        switch ( ptrack->type() ) {
        case LHCb::Track::Types::Long:
          rected = IMCReconstructed::ChargedLong;
          break;
        case LHCb::Track::Types::Downstream:
          rected = IMCReconstructed::ChargedDownstream;
          break;
        case LHCb::Track::Types::Upstream:
          rected = IMCReconstructed::ChargedUpstream;
          break;

          // Other enum values not covered by the original logic
        case LHCb::Track::Types::Unknown:
        case LHCb::Track::Types::Velo:
        case LHCb::Track::Types::Ttrack:
        case LHCb::Track::Types::Muon:
        default:
          break;
        }

        if ( msgLevel( MSG::DEBUG ) ) debug() << "      rected = " << rected << endmsg;

        break;
      }
    }
  } else ///< for neutrals
  {
    auto pNPPAsct = Object2FromMC<LHCb::ProtoParticle, GaudiTool>( this, Particle2MCMethod::NeutralPP,
                                                                   LHCb::ProtoParticleLocation::Neutrals );
    if ( auto r = pNPPAsct.range( iMCstable ); !r.empty() ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "    has an associated neutral ProtoParticle." << endmsg;

      rected = IMCReconstructed::Neutral;
    }

    if ( msgLevel( MSG::DEBUG ) ) debug() << "      rected = " << rected << endmsg;
  }

  if ( msgLevel( MSG::DEBUG ) ) debug() << "  returning " << IMCReconstructed::text( rected ) << endmsg;

  return rected;
}

//=============================================================================
