/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AsctAlgorithm.h"
#include "Event/MCParticle.h"
#include "Event/Particle.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/IToolSvc.h"
#include "Kernel/Particle2MCLinker.h"
#include <memory>
#include <string>

using namespace LHCb;

//-----------------------------------------------------------------------------
// Implementation file for class : Particle2MCChi2
//
// 17/05/2002 : Philippe Charpentier
//-----------------------------------------------------------------------------

/** @class Particle2MCChi2 Particle2MCChi2.h
 *
 *
 *  @author Philippe Charpentier
 *  @date   17/05/2002
 */
class Particle2MCChi2 : public GaudiAlgorithm {
public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode execute() override; ///< Algorithm execution

private:
  Gaudi::Property<std::vector<std::string>> m_inputData{this, "InputData", {}}; ///< location of input data in the TES
  Gaudi::Property<double>                   m_chi2{this, "Chi2Cut", 100.};      ///< Chi2 maximum
};

// Declaration of the Algorithm Factory

DECLARE_COMPONENT( Particle2MCChi2 )

#define _verbose                                                                                                       \
  if ( msgLevel( MSG::VERBOSE ) ) verbose()
#define _debug                                                                                                         \
  if ( msgLevel( MSG::DEBUG ) ) debug()
#define _info                                                                                                          \
  if ( msgLevel( MSG::INFO ) ) info()

//=============================================================================
// Main execution
//=============================================================================
StatusCode Particle2MCChi2::execute() {

  _debug << "==> Execute" << endmsg;

  //////////////////////////////////
  // Loop on Particles containers //
  //////////////////////////////////

  for ( const auto& inp : m_inputData ) {
    // Create a linker table
    Object2MCLinker<LHCb::Particle> p2MCLink( this, Particle2MCMethod::WithChi2, m_inputData );
    LHCb::LinksByKey* linkerTable = LHCb::LinksByKey::fetchFromOrCreateOnTES<LHCb::ProtoParticle, LHCb::MCParticle>(
        evtSvc(), inp + Particle2MCMethod::extension[Particle2MCMethod::Chi2],
        LHCb::LinksByKey::Order::decreasingWeight );
    if ( !linkerTable ) continue;

    // Get Particles
    const auto* parts = getIfExists<Particles>( inp );
    if ( !parts ) continue;
    int npp  = parts->size();
    int nass = 0;
    int nrel = 0;
    _debug << "    " << npp << " Particles retrieved from " << inp << endmsg;

    for ( const auto& part : *parts ) {
      _verbose << "    Particle " << objectName( part );
      bool   found   = false;
      double minChi2 = 0;
      for ( const auto& [mcPart, weight] : p2MCLink.range( part ) ) {
        if ( weight <= m_chi2 ) {
          if ( !found ) { _verbose << " associated to MCParts"; }
          _verbose << " - " << mcPart.key();
          if ( linkerTable ) linkerTable->link( part, &mcPart, weight );
          found = true;
          nrel++;
        }
        if ( minChi2 == 0 || weight < minChi2 ) minChi2 = weight;
      }
      if ( found ) {
        ++nass;
      } else {
        _verbose << " not associated to any MCPart, minChi2 = " << minChi2;
      }
      _verbose << endmsg;
    }
    _debug << "Out of " << npp << " Particles in " << inp << ", " << nass << " are associated, " << nrel
           << " relations found" << endmsg;
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
