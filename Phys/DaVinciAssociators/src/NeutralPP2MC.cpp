/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureUtils/CaloFuture2MC.h"
#include "Event/CaloCluster.h"
#include "Event/CaloHypo.h"
#include "Event/MCParticle.h"
#include "Event/ProtoParticle.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Kernel/PP2MCLocation.h"
#include "Kernel/Particle2MCLinker.h"
#include "Relations/IRelationWeighted.h"
#include "Relations/RelationWeighted1D.h"

/** @class NeutralPP2MC NeutralPP2MC.cpp
 *
 *  The algorithm for building the relation table for
 *  MC truth information for neutrals protoparticles.
 *
 *  The major properties of the algorithm :
 *
 *   - "InputData"
 *      The default value is ProtoParticleLocation::Neutrals
 *      The list of (input) location of ProtoParticles
 *
 *   - "OutputTable"
 *      The defualt valeu is  NeutralPP2MCAsctLocation
 *      The locatioin of (output) relation table for
 *      ProtoParticle -> MCRelation
 *
 *   - "MCCaloTable"
 *      The default value is "Rec/Relations/EcalClusters2MCParticles"
 *      The locatiin in TES the major source of MC information :
 *      the relation table for CaloCluster -> MCParticle relations
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   08/04/2002
 */
using namespace LHCb;

/** @class NeutralPP2MC NeutralPP2MC.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2008-09-25
 */
class NeutralPP2MC : public GaudiAlgorithm {
public:
  StatusCode execute() override;
  /** Standard constructor
   *  @param name name of the algorithm
   *  @param svc  service locator
   */
  NeutralPP2MC( const std::string& name, ISvcLocator* svc );
  StatusCode initialize() override;
  /** standard execution of the algorithm
   *  @return StatusCode
   */

private:
  Gaudi::Property<std::string>              m_outputTable{this, "OutputTable",
                                             LHCb::ProtoParticle2MCLocation::Neutrals}; ///< location of relations table
  Gaudi::Property<std::vector<std::string>> m_inputData{this, "InputData", {}}; ///< location of input data in the TES
  std::string                               m_mcTable;
};

DECLARE_COMPONENT( NeutralPP2MC )

NeutralPP2MC::NeutralPP2MC( const std::string& name, ISvcLocator* svc ) : GaudiAlgorithm( name, svc ), m_mcTable() {
  declareProperty( "MCCaloTable", m_mcTable );
}

// ============================================================================

StatusCode NeutralPP2MC::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize();
  if ( !sc.isSuccess() ) return sc;

  // property setting for Neutral is based on context()
  // default setup in initialize() to please the linker design
  std::string out( context() );
  std::transform( context().begin(), context().end(), out.begin(), ::toupper );
  if ( out == "HLT" ) {
    if ( m_inputData.empty() ) m_inputData.value() = {LHCb::ProtoParticleLocation::HltNeutrals};
    if ( m_outputTable.empty() ) m_outputTable = "Relations/" + LHCb::ProtoParticleLocation::HltNeutrals;
    ;
    if ( m_mcTable.empty() ) m_mcTable = "Relations/" + LHCb::CaloClusterLocation::DefaultHlt;
  } else {
    if ( m_inputData.empty() ) m_inputData.value() = {LHCb::ProtoParticleLocation::Neutrals};
    if ( m_outputTable.empty() ) m_outputTable = "Relations/" + LHCb::ProtoParticleLocation::Neutrals;
    ;
    if ( m_mcTable.empty() ) m_mcTable = "Relations/" + LHCb::CaloClusterLocation::Default;
  }

  return StatusCode::SUCCESS;
}

// ============================================================================
/** standard execution of the algorithm
 *  @see     Algorithm
 *  @see    IAlgorithm
 *  @return StatusCode
 */
// ============================================================================
StatusCode NeutralPP2MC::execute() {

  if ( msgLevel( MSG::DEBUG ) )
    debug() << "Execute for context = " << context() << "::  inputData   = (" << m_inputData << ")"
            << "::  output Relation table =  " << m_outputTable << "::  input  Relation table =  " << m_mcTable
            << endmsg;

  // ProtoParticle -> MCParticle relations   (MC output)
  typedef RelationWeighted1D<LHCb::ProtoParticle, LHCb::MCParticle, double> Table;

  // CaloCluster -> MCParticle relations     (MC input)
  typedef const LHCb::CaloFuture2MC::IClusterTable MCTable;

  // create protoP<->MC output relation table
  Table* table = nullptr;
  if ( !m_outputTable.empty() ) {
    table = new Table();
    put( table, m_outputTable.value() );
  };
  // get cluster<->MC input relation table
  const MCTable* mcTable = getIfExists<MCTable>( m_mcTable );
  if ( !mcTable ) {
    warning() << "could not retrieve " << m_mcTable << endmsg;
    return StatusCode::SUCCESS; // RETURN
  }

  // Loop on all input containers of ProtoParticles
  for ( const auto& inp : m_inputData ) {

    // get the protoparticles from TES
    const ProtoParticles* protos = getIfExists<ProtoParticles>( inp );
    if ( !protos ) {
      warning() << "could not retrieve " << inp << endmsg;
      return StatusCode::SUCCESS; // RETURN
    }

    // Create a linker table
    const std::string linkContainer = inp + Particle2MCMethod::extension[Particle2MCMethod::NeutralPP];
    LHCb::LinksByKey* linkerTable   = LHCb::LinksByKey::fetchFromOrCreateOnTES<LHCb::ProtoParticle, LHCb::MCParticle>(
        evtSvc(), linkContainer, LHCb::LinksByKey::Order::decreasingWeight );

    if ( !table && !linkerTable ) { continue; } // CONTINUE

    int npp = 0;
    for ( const ProtoParticle* pp : *protos ) {

      if ( !pp ) { continue; }
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "- ProtoParticle : #" << npp << " ( Context = '" << context() << "' )" << endmsg;
      ++npp;
      // loop over all hypotheses for givel protoparticle
      for ( const CaloHypo* hypo : pp->calo() ) {

        if ( !hypo ) { continue; } // CONTINUE

        if ( msgLevel( MSG::DEBUG ) )
          debug() << "  --> CaloHypo : Energy = " << hypo->e() << " : Hypothesis = " << hypo->hypothesis() << endmsg;

        for ( const CaloCluster* cluster : hypo->clusters() ) {
          if ( !cluster ) { continue; } // CONTINUE

          if ( msgLevel( MSG::DEBUG ) ) debug() << "   ---> CaloCluster : Energy = " << cluster->e() << endmsg;

          for ( const auto& rel : mcTable->relations( cluster->seed() ) ) {
            const MCParticle* mcPart = rel.to();
            if ( !mcPart ) { continue; } // CONTINUE

            const double weight = rel.weight() / cluster->e();

            if ( msgLevel( MSG::DEBUG ) )
              debug() << "    ----> MCParticle : Energy " << mcPart->momentum().e()
                      << " : PID = " << mcPart->particleID().pid() << " : Weight " << weight << endmsg;

            if ( table ) {
              table->relate( pp, mcPart, weight ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
            }
            if ( linkerTable ) { linkerTable->link( pp, mcPart, weight ); }

          } // end of loop over MC relations
        }   // end of loop over clusters
      }     // end of loop over hypothesis
    }       // end of loop over protoparticles
  }         // end of loop over input containters

  if ( msgLevel( MSG::DEBUG ) && table ) {
    debug() << "Number of MC links are " << table->relations().size() << endmsg;
  }
  return StatusCode::SUCCESS;
}
