/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef ASCTALGORITHM_H
#define ASCTALGORITHM_H 1

#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/KeyedObject.h"
#include <string>

inline std::string objectName( const KeyedObject<int>* obj ) {
  if ( !obj->hasKey() || !obj->parent() || !obj->parent()->registry() ) return "noKey";
  return obj->parent()->registry()->identifier() + "/" + std::to_string( obj->key() );
}

//=============================================================================
#endif // ASCTALGORITHM_H
