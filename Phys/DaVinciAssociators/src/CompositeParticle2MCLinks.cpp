/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AsctAlgorithm.h"
#include "Event/MCParticle.h"
#include "Event/MCVertex.h"
#include "Event/Particle.h"
#include "Event/Vertex.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/Particle2MCLinker.h"
#include "Kernel/ParticleProperty.h"
#include "Linker/LinkedTo.h"
#include <string>

/** @class CompositeParticle2MCLinks CompositeParticle2MCLinks.h
 *
 *  @author Gerhard Raven
 *  @date   September 2002
 */
class CompositeParticle2MCLinks : public GaudiAlgorithm {
public:
  /// Standard constructor
  CompositeParticle2MCLinks( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  bool isResonance( const LHCb::Particle* ) const;

  // Properties
  bool        m_allowExtraMCPhotons;
  bool        m_inclusiveMode;
  bool        m_skipResonances;
  double      m_maxResonanceLifeTime;
  std::string m_asctMethod;
  bool        m_ignorePID;
  // Local variables
  int                         m_method;
  LHCb::IParticlePropertySvc* m_ppSvc = nullptr;
  int                         m_gamma{22};
  int                         m_nrel{0};
  int                         m_nass{0};
  // Private methods
  bool associateTree( const LHCb::Particle* p, const LHCb::MCParticle* m, LHCb::LinksByKey* linkerTable,
                      Object2MCLinker<LHCb::Particle>& p2MCComp, Object2MCLinker<LHCb::Particle>& p2MCLink );

  bool addDaughters( const LHCb::Particle* m, std::vector<const LHCb::Particle*>& daughters ) const;
  bool addMCDaughters( const LHCb::MCParticle* m, std::vector<const LHCb::MCParticle*>& daughters ) const;
  Gaudi::Property<std::vector<std::string>> m_inputData{this, "InputData", {}}; ///< location of input data in the TES
  Gaudi::Property<std::string>              m_outputTable{this, "OutputTable",
                                             "Phys/Relations/CompPart2MCfromLinks"}; ///< location of relations table
};

using namespace LHCb;

//#define DEEPDEBUG

//-----------------------------------------------------------------------------
// Implementation file for class : CompositeParticle2MCLinks
//
// 03/09/2002 : Gerhard Raven
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory

DECLARE_COMPONENT( CompositeParticle2MCLinks )

#define _verbose                                                                                                       \
  if ( msgLevel( MSG::VERBOSE ) ) verbose()
#define _debug                                                                                                         \
  if ( msgLevel( MSG::DEBUG ) ) debug()
#define _info                                                                                                          \
  if ( msgLevel( MSG::INFO ) ) info()

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CompositeParticle2MCLinks::CompositeParticle2MCLinks( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator ) {
  declareProperty( "AllowExtraMCPhotons", m_allowExtraMCPhotons = true );
  declareProperty( "inclusiveMode", m_inclusiveMode = false );
  declareProperty( "skipResonances", m_skipResonances = false );
  declareProperty( "maxResonanceLifeTime", m_maxResonanceLifeTime = 1.e-18 * Gaudi::Units::s );
  declareProperty( "AssociationMethod", m_asctMethod = "Links" );
  declareProperty( "IgnorePID", m_ignorePID = false );
}

//=============================================================================
// Initialisation. Check parameters
//=============================================================================
StatusCode CompositeParticle2MCLinks::initialize() {

  _debug << "==> Initialise" << endmsg;

  StatusCode sc = GaudiAlgorithm::initialize();
  if ( !sc.isSuccess() ) return sc;

  m_asctMethod = "/" + m_asctMethod;
  m_method     = Particle2MCMethod::Links;
  for ( int i = Particle2MCMethod::No; Particle2MCMethod::Max != i; i++ ) {
    if ( Particle2MCMethod::extension[i] == m_asctMethod ) {
      m_method = i;
      break;
    }
  }

  _info << "Composite association uses " << Particle2MCMethod::extension[m_method] << " as input" << endmsg;

  m_ppSvc = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc" );
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode CompositeParticle2MCLinks::execute() {

  _debug << "==> Execute" << endmsg;

  if ( !m_outputTable.empty() ) {
    return Error( "Usage of Relations in Particle Associators is obsolete, use Linkers instead" );
  }

  // This is a link helper for the underlying associator (default : Links)
  Object2MCLinker<LHCb::Particle> p2MCLink( this, m_method, m_inputData );
  // This is a linker to itself, to check if Particles have already been associated
  Object2MCLinker<LHCb::Particle> p2MCComp( this, "", Particle2MCMethod::extension[Particle2MCMethod::Composite],
                                            m_inputData );

  // get MCParticles
  MCParticles* mcParts = get<MCParticles>( MCParticleLocation::Default );

  //////////////////////////////////
  // Loop on Particles containers //
  //////////////////////////////////

  for ( auto const& inp : m_inputData ) {
    // Get Particles
    // create a Linker table for that Particle container
    const std::string linkerContainer = inp + Particle2MCMethod::extension[Particle2MCMethod::Composite];
    LHCb::LinksByKey* linkerTable     = LHCb::LinksByKey::fetchFromOrCreateOnTES<LHCb::Particle, LHCb::MCParticle>(
        evtSvc(), linkerContainer, LHCb::LinksByKey::Order::decreasingWeight );

    // If there is no need to fill in any table for that container
    //    just go to the next
    if ( !linkerTable ) continue;

    Particles const* parts = getIfExists<Particles>( inp );
    if ( !parts ) continue;
    int npp = parts->size();
    m_nrel  = 0;
    m_nass  = 0;

    _debug << npp << " Particles retrieved from " << inp << endmsg;

    // loop on Parts and MCParts to match them
    for ( const auto* part : *parts ) {
      // If the particle is apready in the table, don't look for it
      if ( p2MCComp.range( part ).empty() ) continue;

      // Check for direct association
      bool foundDirectAssociation = false;

      auto mcps = p2MCLink.range( part );
      if ( !mcps.empty() ) { // copy from underlying associator
        const ParticleID& idP     = part->particleID();
        bool              counted = false;
        _verbose << "Direct association for " << objectName( part ) << " (" << idP.pid() << ") "
                 << " -> MCParts";
        for ( const auto& [mcp, weight] : mcps ) {
          const ParticleID& idM = mcp.particleID();
          if ( m_ignorePID || idP.pid() == idM.pid() || ( part->charge() == 0 && idP.abspid() == idM.abspid() ) ) {
            if ( !counted ) {
              ++m_nass;
              counted = true;
            }
            _verbose << " - " << mcp.key() << " (" << idM.pid() << ")";
            if ( linkerTable ) linkerTable->link( part, &mcp );
            ++m_nrel;
          } else {
            _verbose << " - " << mcp.key() << " Bad PID (" << idM.pid() << ")";
          }
        }
        _verbose << endmsg;
      } // if mcps.empty()

      //
      // alternate approach for direct link between LHCb::Particle -> LHCb::MCParticle
      //
      if ( mcps.empty() ) {
        _verbose << "try alternate approach for direct association for particle with PID " << part->particleID().pid()
                 << endmsg;
        SmartDataPtr<LHCb::LinksByKey> p2mcpLinks( evtSvc(), LHCb::LinksByKey::linkerName( inp ) );
        if ( p2mcpLinks ) p2mcpLinks->resolveLinks( evtSvc() );
        const LinkedTo<LHCb::MCParticle> p2MCPTable{p2mcpLinks};
        if ( p2MCPTable ) {
          auto p2MCPRange = p2MCPTable.weightedRange( part );
          verbose() << "found #relations " << p2MCPRange.size() << endmsg;
          for ( auto const& [mcPart, weight] : p2MCPRange ) {
            foundDirectAssociation = true;
            verbose() << "particle related to MCPart with pid " << mcPart.particleID().pid() << " weight " << weight
                      << endmsg;
            if ( linkerTable ) linkerTable->link( part, &mcPart );
            ++m_nrel;
          } // for iRel
        }   // if Table

      } // if !mcp

      if ( !foundDirectAssociation && part->daughters().size() != 0 ) {
        // If the particle has no daughters, useless to check matching daughters
        for ( const auto& m : *mcParts ) {
          if ( associateTree( part, m, linkerTable, p2MCComp, p2MCLink ) ) break;
        }
      } // if

      if ( !foundDirectAssociation && part->daughters().empty() ) {
        _verbose << "Particle " << objectName( part ) << " not considered: no direct asct and no daughters" << endmsg;
      } // if
    }
    _debug << "Out of " << npp << " Particles in " << inp << ", " << m_nass << " are associated, " << m_nrel
           << " relations found" << endmsg;
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
//  associateTree auxiliary method
//=============================================================================
bool CompositeParticle2MCLinks::associateTree( const Particle* p, const MCParticle* m, LHCb::LinksByKey* linkerTable,
                                               Object2MCLinker<LHCb::Particle>& p2MCComp,
                                               Object2MCLinker<LHCb::Particle>& p2MCLink ) {
  bool              s = false;
  std::string       asctType;
  const ParticleID& idP = p->particleID();
  const ParticleID& idM = m->particleID();
  if ( m_ignorePID || idP.pid() == idM.pid() || ( p->charge() == 0 && idP.abspid() == idM.abspid() ) ) {
#ifdef DEEPDEBUG
    _verbose << " Checking particle " << objectName( p ) << " (" << idP.pid() << ") "
             << " and MCPart " << m->key() << " (" << idM.pid() << ") potential match " << endmsg;
#endif
    // Check if these particles have already been associated
    if ( p2MCComp.checkAssociation( p, m ) ) {
      _verbose << "     Association already registered between Particle " << objectName( p ) << " and MCPart "
               << m->key() << endmsg;
      return true;
    }
    if ( p->daughters().size() == 0 ) {
#ifdef DEEPDEBUG
      _verbose << "     No daughter, check underlying particle associator" << endmsg;
#endif
      s        = p2MCLink.checkAssociation( p, m );
      asctType = "Direct";
    } else {
      asctType = "Composite";
      std::vector<const Particle*> dau;
      addDaughters( p, dau );

      // Get a vector of products of the MCParticle
      // check the sum of all endVertices???
      std::vector<const MCParticle*> mcdau;
      if ( addMCDaughters( m, mcdau ) ) {

        std::vector<const Particle*>::const_iterator i;
        // Loop on the Particle's daughters
        for ( i = dau.begin(); i != dau.end(); ++i ) {
          // Loop on MCParticles daughters and check if they are associated
          auto j = std::find_if( mcdau.begin(), mcdau.end(), [&]( const auto& k ) {
            return associateTree( *i, k, linkerTable, p2MCComp, p2MCLink );
          } );
          // doesn't match any MCParticle -- give up
          if ( j == mcdau.end() ) break;
          // Match found, remove it from the MC daughters
          mcdau.erase( j );
        }

        if ( m_allowExtraMCPhotons ) { // maybe add an energy threshold
                                       // for extraneous photons?
          while ( !mcdau.empty() && mcdau.back()->particleID().pid() == m_gamma ) mcdau.pop_back();
        }

        // all matched up, nothing left
        s = ( i == dau.end() && ( m_inclusiveMode || mcdau.empty() ) );
      }
    }
  }
  if ( s ) {
    _verbose << asctType << " association found: " << objectName( p ) << " (" << idP.pid() << ") "
             << " -> MCPart - " << m->key() << " (" << idM.pid() << ")" << endmsg;
    if ( linkerTable ) linkerTable->link( p, m );
    ++m_nrel;
    ++m_nass;
  }
  return s;
}

bool CompositeParticle2MCLinks::addDaughters( const Particle* p, std::vector<const Particle*>& daughters ) const {
  for ( const auto& k : p->daughters() ) {
    // if resonances should be skipped add daughters instead
    if ( isResonance( k ) && m_skipResonances && addDaughters( k, daughters ) )
      ;
    else
      daughters.push_back( k );
  }
  return true;
}

bool CompositeParticle2MCLinks::addMCDaughters( const MCParticle* m, std::vector<const MCParticle*>& daughters ) const {
  const auto& vMC = m->endVertices();
  if ( !vMC.empty() && vMC[0] ) {
    for ( const auto& k : vMC[0]->products() ) {

      // Use lifetime to determine if MCParticle is a resonance
      const LHCb::ParticleProperty* partProp  = m_ppSvc->find( ( k )->particleID() );
      bool                          resonance = ( partProp && partProp->lifetime() < m_maxResonanceLifeTime );

      // if resonances should be skipped add daughters instead
      if ( resonance && m_skipResonances && addMCDaughters( k, daughters ) )
        ;
      else
        daughters.push_back( k );
    }
    return true;
  }
  return false;
}

bool CompositeParticle2MCLinks::isResonance( const LHCb::Particle* part ) const {
  const LHCb::ParticleProperty* partProp = m_ppSvc->find( part->particleID() );
  return partProp->lifetime() < m_maxResonanceLifeTime;
}
