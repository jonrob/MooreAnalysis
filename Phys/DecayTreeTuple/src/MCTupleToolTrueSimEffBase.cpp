/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "boost/regex.hpp"
#include "gsl/gsl_sys.h"

// local
#include "Event/MCParticle.h"
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"
#include "MCTupleToolTrueSimEffBase.h"

using namespace LHCb;
//-----------------------------------------------------------------------------
// Implementation file for class : MCTupleToolTrueSimEffBase
//
// 2021-09-02 : Ross Hunter
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MCTupleToolTrueSimEffBase::MCTupleToolTrueSimEffBase( const std::string& type, const std::string& name,
                                                      const IInterface* parent )
    : TupleToolBase( type, name, parent ) {
  declareInterface<IMCParticleTupleTool>( this );
}

//=============================================================================
// initialize
//=============================================================================
StatusCode MCTupleToolTrueSimEffBase::initialize() {
  if ( !TupleToolBase::initialize() ) return StatusCode::FAILURE;

  if ( !m_triggerList.empty() ) compileTriggerLineList( m_triggerList );

  return StatusCode::SUCCESS;
}

void MCTupleToolTrueSimEffBase::fetchLHCbIDsFromMCParticle( const LHCb::MCParticle& mcp,
                                                            const LHCb::LinksByKey* mc2IdLink,
                                                            WeightedIDsByMCPTrack&  weighted_ids_by_mcp_track ) {
  // Recursive function to iterate over each child of the MCParticle and collect the LHCbIDs in map

  // Add this track's LHCbIDs to the map
  std::map<LHCb::LHCbID, float> this_track_map_of_ids{};
  mc2IdLink->applyToAllLinks( [&this_track_map_of_ids, &mcp]( unsigned int id, int mcPartKey, float weight ) {
    if ( mcPartKey == mcp.key() ) { this_track_map_of_ids.insert( {LHCb::LHCbID( id ), weight} ); }
  } );
  if ( this_track_map_of_ids.size() > 0 ) { weighted_ids_by_mcp_track.emplace_back( this_track_map_of_ids ); }

  // Now iterate through children and add their LHCbIDs to the map
  // Use the MCParticle::endVertices method to get vertices on this track. Those that are are isDecay() are decay
  // vertices
  // TODO should we consider other types of vertex? I guess they are just radiating photons, material interactions etc.
  const auto& endVertices = mcp.endVertices();
  for ( const auto& decayVertex : endVertices ) {
    if ( decayVertex->isDecay() ) {
      const auto& decayProducts = decayVertex->products();
      for ( const auto& childParticle : decayProducts ) {
        fetchLHCbIDsFromMCParticle( *childParticle, mc2IdLink, weighted_ids_by_mcp_track );
      }
    }
  }

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Collected " << weighted_ids_by_mcp_track.size() << " IDs for MCParticle with ID " << mcp.particleID()
            << endmsg;
  }
}

bool MCTupleToolTrueSimEffBase::match_to_true_signal( const IDsByTrack&            ids_by_candidate_track,
                                                      const WeightedIDsByMCPTrack& weighted_ids_by_mcp_track ) {

  if ( ( weighted_ids_by_mcp_track.size() == 0 ) || ( ids_by_candidate_track.size() == 0 ) ) {
    return false; // Can't be matched if either of these are the case
  }

  auto reco_track_matches_to_mcp = [& m_match_threshold = m_match_threshold,
                                    &weighted_ids_by_mcp_track]( const IDvec& reco_track_IDs ) {
    if ( reco_track_IDs.size() == 0 ) { return false; }
    for ( const auto& mcp_track_IDs : weighted_ids_by_mcp_track ) {
      float n_matching_IDs{0.f};
      for ( const auto& id : reco_track_IDs ) {
        const auto iter = mcp_track_IDs.find( id );
        if ( iter != mcp_track_IDs.end() ) { n_matching_IDs += iter->second; }
      }
      float reco_match_frac = n_matching_IDs / reco_track_IDs.size();
      if ( reco_match_frac >= m_match_threshold ) { return true; }
    }
    return false;
  };

  bool all_tracks_match_to_mcp = true;
  for ( const auto& ids_this_track : ids_by_candidate_track ) {
    all_tracks_match_to_mcp &= reco_track_matches_to_mcp( ids_this_track );
  }

  return all_tracks_match_to_mcp;
}

bool MCTupleToolTrueSimEffBase::compileTriggerLineList( const std::vector<std::string>& line_names ) {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Compiling list of trigger lines" << endmsg;

  boost::regex hlt1( "Hlt1.*Decision" );
  boost::regex hlt2( "Hlt2.*Decision" );

  for ( const auto& line_name : line_names ) {
    if ( boost::regex_match( line_name, hlt1 ) ) {
      m_hlt1_line_names.push_back( line_name );
    } else if ( boost::regex_match( line_name, hlt2 ) ) {
      m_hlt2_line_names.push_back( line_name );
    } else {
      error() << "List member ``" << line_name
              << "'' does not match any known pattern. Have you forgotten the trailing 'Decision'? " << endmsg;
    }
  }

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << " ==== HLT1 ==== " << endmsg;
    for ( const auto& line_name : m_hlt1_line_names ) debug() << " " << line_name;
    debug() << endmsg;
    debug() << " ==== HLT2 ==== " << endmsg;
    for ( const auto& line_name : m_hlt2_line_names ) debug() << " " << line_name;
    debug() << endmsg;
    debug() << " ==== Compiled list ====" << endmsg;
  }

  return true;
}
