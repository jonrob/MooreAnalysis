/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "DecayTreeTupleBase/TupleToolBase.h"

#include "Kernel/IBackgroundCategory.h"
#include "Kernel/IParticleTupleTool.h"

#include "Event/Particle.h"

#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"

/**
 * \brief Fill the info from IBackgroundCategory
 *
 * An answer is only filled for composite particles.
 *
 * IBackgroundCategory controlled by property <b>IBackgroundCatagoryType</b>, default "BackgroundCategory".
 *
 * head_BKGCAT : category.
 *
 * \sa DecayTreeTuple
 *
 *  @author Jeremie Borel
 *  @date   2007-11-07
 */
class TupleToolMCBackgroundInfo : public TupleToolBase, virtual public IParticleTupleTool {

public:
  TupleToolMCBackgroundInfo( const std::string& type, const std::string& name, const IInterface* parent );
  StatusCode initialize() override;
  StatusCode fill( const LHCb::Particle*, const LHCb::Particle*, const std::string&, Tuples::Tuple&,
                   IGeometryInfo const& ) override;

private:
  std::vector<std::string>          m_backCatTypes;
  std::vector<IBackgroundCategory*> m_bkgs;
};

using namespace LHCb;

TupleToolMCBackgroundInfo::TupleToolMCBackgroundInfo( const std::string& type, const std::string& name,
                                                      const IInterface* parent )
    : TupleToolBase( type, name, parent ) {
  declareInterface<IParticleTupleTool>( this );
  m_backCatTypes.push_back( "BackgroundCategoryViaRelations" );
  m_backCatTypes.push_back( "BackgroundCategory" );
  declareProperty( "IBackgroundCategoryTypes", m_backCatTypes );
}

StatusCode TupleToolMCBackgroundInfo::initialize() {
  const StatusCode sc = TupleToolBase::initialize();
  if ( sc.isFailure() ) return sc;
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Background Category tools " << m_backCatTypes << endmsg;
  for ( std::vector<std::string>::const_iterator iT = m_backCatTypes.begin(); iT != m_backCatTypes.end(); ++iT ) {
    m_bkgs.push_back( tool<IBackgroundCategory>( *iT, *iT, this ) );
  }
  return sc;
}

StatusCode TupleToolMCBackgroundInfo::fill( const Particle* headP, const Particle* P, const std::string& head,
                                            Tuples::Tuple& tuple, IGeometryInfo const& ) {
  StatusCode        sc     = StatusCode::SUCCESS;
  const std::string prefix = fullName( head );
  Assert( P && !m_bkgs.empty(), "This should not happen :(" );
  if ( !P->isBasicParticle() ) {
    IBackgroundCategory::categories cat = IBackgroundCategory::Undefined;
    for ( std::vector<IBackgroundCategory*>::const_iterator iT = m_bkgs.begin(); iT != m_bkgs.end(); ++iT ) {
      cat = ( *iT )->category( P, headP ).category;
      if ( cat != IBackgroundCategory::Undefined ) break;
    }
    if ( msgLevel( MSG::DEBUG ) ) debug() << "BackgroundCategory decision for " << prefix << " : " << cat << endmsg;
    sc = tuple->column( prefix + "_BKGCAT", (int)cat );
  }
  return sc;
}

// Declaration of the Tool Factory
// actually acts as a using namespace TupleTool
DECLARE_COMPONENT( TupleToolMCBackgroundInfo )
