/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MCTUPLETOOLTRUESIMEFFHLT2_H
#define MCTUPLETOOLTRUESIMEFFHLT2_H 1

#include "Event/HltDecReports.h"
#include "MCTupleToolTrueSimEffBase.h"

class MCTupleToolTrueSimEffHLT2 : public MCTupleToolTrueSimEffBase {
public:
  /// Standard constructor
  MCTupleToolTrueSimEffHLT2( const std::string& type, const std::string& name, const IInterface* parent );
  StatusCode initialize() override;
  StatusCode fill( const LHCb::MCParticle*, const LHCb::MCParticle*, const std::string&, Tuples::Tuple& ) override;

private:
  void fetchLHCbIDsFromRecoParticle( const LHCb::Particle& part, IDsByTrack& bucket_of_ids );

private:
  std::vector<std::unique_ptr<DataObjectReadHandle<LHCb::Particles>>> m_candidateHandles;
  Gaudi::Property<std::vector<std::string>> m_candidateList{this, "CandidateLocations", {}, ""};
  DataObjectReadHandle<LHCb::HltDecReports> m_hlt2DecReportDh = {this, "Hlt2DecReports",
                                                                 LHCb::HltDecReportsLocation::Hlt2Default};
};
#endif // MCTUPLETOOLTRUESIMEFFHLT2_H
