/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MCTUPLETOOLTRUESIMEFFHLT1_H
#define MCTUPLETOOLTRUESIMEFFHLT1_H 1

#include "Event/HltSelReports.h"
#include "MCTupleToolTrueSimEffBase.h"

class MCTupleToolTrueSimEffHLT1 : public MCTupleToolTrueSimEffBase {
public:
  /// Standard constructor
  MCTupleToolTrueSimEffHLT1( const std::string& type, const std::string& name, const IInterface* parent );
  StatusCode fill( const LHCb::MCParticle*, const LHCb::MCParticle*, const std::string&, Tuples::Tuple& ) override;

private:
  bool fetchLHCbIDsFromHOS( const SmartRef<LHCb::HltObjectSummary>& hltObjectSummary, IDsByTrack& bucket_of_ids );
  std::map<IDsByTrack, int> getLHCbIDsByTrigCand( const LHCb::HltObjectSummary& topLevelSummary );

private:
  DataObjectReadHandle<LHCb::HltSelReports> m_hltSelReportDh = {this, "HltSelReports",
                                                                LHCb::HltSelReportsLocation::Default};
};
#endif // MCTUPLETOOLTRUESIMEFFHLT1_H
