/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "boost/algorithm/string/replace.hpp"
#include "boost/regex.hpp"
#include "gsl/gsl_sys.h"

// local
#include "Event/MCParticle.h"
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"
#include "MCTupleToolTrueSimEffHLT2.h"

using namespace LHCb;
//-----------------------------------------------------------------------------
// Implementation file for class : MCTupleToolTrueSimEffHLT2
//
// 2021-09-02 : Ross Hunter
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( MCTupleToolTrueSimEffHLT2 )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MCTupleToolTrueSimEffHLT2::MCTupleToolTrueSimEffHLT2( const std::string& type, const std::string& name,
                                                      const IInterface* parent )
    : MCTupleToolTrueSimEffBase( type, name, parent ) {
  declareInterface<IMCParticleTupleTool>( this );
}

//=============================================================================
// initialize
//=============================================================================
StatusCode MCTupleToolTrueSimEffHLT2::initialize() {
  if ( !MCTupleToolTrueSimEffBase::initialize() ) return StatusCode::FAILURE;

  for ( auto const& location : m_candidateList ) {
    std::cout << "Adding input TES location " << location << std::endl;
    m_candidateHandles.push_back( std::make_unique<DataObjectReadHandle<LHCb::Particles>>( location, this ) );
    bool declared = false;
    for ( auto const& line : m_triggerList ) {
      std::string line_without_decision = line;
      boost::replace_all( line_without_decision, "Decision", "" );
      if ( location.find( line_without_decision ) != std::string::npos ) {
        declareProperty( line_without_decision + "_Candidates", *( m_candidateHandles.back() ) );
        declared = true;
      }
    }
    if ( !declared ) {
      error() << "Passed trigger candidates at " << location
              << " which dont match to any of the lines in m_triggerList." << endmsg;
      return StatusCode::FAILURE;
    }
  }

  return StatusCode::SUCCESS;
}

void MCTupleToolTrueSimEffHLT2::fetchLHCbIDsFromRecoParticle( const LHCb::Particle& part, IDsByTrack& bucket_of_ids ) {
  // Recursive function to extract all the LHCbIDs from a persisted trigger candidate

  if ( part.isBasicParticle() ) {
    // We can just get the protoparticle and its IDs
    if ( part.charge() == 0 ) {
      warning() << "Particle with ID " << part.particleID()
                << " is neutral, so has no track. Adding empty container of LHCbIDs to this candidate for this track."
                << endmsg;
      bucket_of_ids.emplace_back();
    } else {
      bucket_of_ids.emplace_back( part.proto()->track()->lhcbIDs() );
    }
  } else {
    const auto& children = part.daughters();
    for ( const auto& child : children ) { fetchLHCbIDsFromRecoParticle( *child, bucket_of_ids ); }
  }
}

//=============================================================================
// Fill
//=============================================================================
StatusCode MCTupleToolTrueSimEffHLT2::fill( const LHCb::MCParticle*, const LHCb::MCParticle* mcp,
                                            const std::string& head, Tuples::Tuple& tuple ) {
  const std::string prefix = fullName( head );

  // TODO need to figure out why it looks like this is all getting called twice.
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Filling HLT2 DecisionTrueSim branches for mcp with ID " << mcp->particleID() << endmsg;
  }

  if ( mcp == nullptr ) {
    warning() << "Particle is NULL. Exiting." << endmsg;
    return StatusCode::FAILURE;
  }

  // Get the LHCbIDs from the MCParticle
  LHCb::LinksByKey* mcp_id_link = m_mc2IdLinkDh.get();

  // TODO is weight always =1? If so a simpler vector will suffice.
  WeightedIDsByMCPTrack weighted_mcp_IDs{};
  fetchLHCbIDsFromMCParticle( *mcp, mcp_id_link, weighted_mcp_IDs );

  for ( const auto& decision_name : m_hlt2_line_names ) {
    // individual Hlt trigger lines
    bool matched_on_this_line{false};

    // Check first if there was a positive trigger decision, then can proceed to matching to true signal on the
    // candidate if so Have to check this as candidates are saved to TES whether decision is positive or not
    bool                       trigger_decision = false;
    const LHCb::HltDecReports* decReports       = m_hlt2DecReportDh.get();
    for ( LHCb::HltDecReports::Container::const_iterator it = decReports->begin(); it != decReports->end(); ++it ) {
      if ( ( it->first == decision_name ) ) {
        trigger_decision = (bool)it->second.decision();
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << "Found decReport for " << decision_name << ". Decision = " << it->second.decision();
          debug() << ( trigger_decision ? "Proceeding to " : "Will not attempt to " )
                  << "match the trigger candidate to the true simulated signal." << endmsg;
        }
      }
    }

    if ( trigger_decision ) {
      std::string line_name = decision_name;
      boost::replace_first( line_name, "Decision", "" );

      for ( auto& candHandle : m_candidateHandles ) {
        if ( !candHandle->isValid() ) continue;

        if ( candHandle->objKey().find( line_name ) != std::string::npos ) {
          LHCb::Particles* candidates = candHandle->get();
          if ( msgLevel( MSG::DEBUG ) ) {
            debug() << "There are " << candidates->size() << " candidates in " << candHandle->objKey() << " for "
                    << decision_name << endmsg;
          }

          std::map<IDsByTrack, int> idsByCandidateMap{};
          int                       iCand = 0;
          for ( const auto& cand : *candidates ) {
            IDsByTrack ids_by_track{};
            fetchLHCbIDsFromRecoParticle( *cand, ids_by_track );
            if ( !idsByCandidateMap.insert( std::pair<IDsByTrack, int>( ids_by_track, iCand ) ).second ) {
              warning() << "Insertion failed. Seems this set of LHCbIDs is already there." << endmsg;
            }
            iCand++;
          }

          for ( auto const& [trig_cand_IDs, i] : idsByCandidateMap ) {
            matched_on_this_line |= match_to_true_signal( trig_cand_IDs, weighted_mcp_IDs );
          }
        } // if candidates this line match the trigger line
      }   // loop over candidates from all lines
    } else {
      // Trigger decision is negative
      matched_on_this_line = false;
    }

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "For trigger with name = " << decision_name << ", TrueSim decision = " << matched_on_this_line
              << " on MCParticle with ID " << mcp->particleID() << endmsg;
    }
    if ( !tuple->column( prefix + "_" + decision_name + "TrueSim", matched_on_this_line ) ) {
      return StatusCode::FAILURE;
    }
  } // Loop over trigger lines

  if ( msgLevel( MSG::DEBUG ) ) { debug() << "Filled branches for " << prefix << " Hlt2" << endmsg; }

  return StatusCode::SUCCESS;
}
