/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MCTUPLETOOLTRUESIMEFFBASE_H
#define MCTUPLETOOLTRUESIMEFFBASE_H 1

#include "DecayTreeTupleBase/TupleToolBase.h"
#include "GaudiKernel/DataObjectHandle.h"
#include "Kernel/IMCParticleTupleTool.h" // Interface
#include "Kernel/Particle2MCLinker.h"

/** @class MCTupleToolTrueSimEffBase MCTupleToolTrueSimEffBase.h
 *
 * Add "matched-to-true-simulated-signal" decision flags to the tuple, as well as other related information on the
 * trigger candidate - MCParticle matching
 *
 * For each event and for each trigger line, matching is done by extracting the LHCbIDs (hits) from each track
 * of the composite signal MCParticle and each track in the trigger candidate. The hits of each trigger candidate track
 * are then compared against all the tracks in the MCParticle for overlapping hits. If there is greater than or equal to
 * <m_match_threshold> fraction of the trigger candidate hits overlapping with the MCParticle in that track, that track
 * is declared to be matched. For the trigger candidate to be matched to the MCParticle, we require that every
 * track sufficiently matches to a track in the MCParticle. For example: if a hypothetical TwoTrackLine combines two
 * tracks in a Bs -> JpsiPhi event, and the two tracks of this TwoTrackLine candidate match the two true muons from
 * the Jpsi, then for this event Jpsi_TwoTrackDecisionTrueSim = 1.
 *
 * Extraction of LHCbIDs from the trigger candidate differs in implementation for HLT1 and HLT2, hence there are two
 * inheriting classes: MCTupleToolTrueSimEffHLT1 and MCTupleToolTrueSimEffHLT2. The former (latter) gets LHCbIDs from
 * the SelReports (persisted candidates). These differing data dependencies require a different derived class.
 *
 * See https://doi.org/10.1051/epjconf/202125104024 for further description of the algorithm and results on a simple
 * test case. An older version of the algorithm is described there, with the current algorithm proposed as an
 * improvement in the "Limitations" section.
 *
 * \sa MCDecayTreeTuple, MCTupleToolTrueSimEffHLT1, MCTupleToolTrueSimEffHLT2
 *
 *  @author Ross Hunter
 *  @date   2021-09-02
 */

using IDvec                 = std::vector<LHCb::LHCbID>;
using IDsByTrack            = std::vector<IDvec>;
using WeightedIDsByMCPTrack = std::vector<std::map<LHCb::LHCbID, float>>;
class MCTupleToolTrueSimEffBase : public TupleToolBase, virtual public IMCParticleTupleTool {
public:
  /// Standard constructor
  MCTupleToolTrueSimEffBase( const std::string& type, const std::string& name, const IInterface* parent );
  StatusCode         initialize() override;
  virtual StatusCode fill( const LHCb::MCParticle*, const LHCb::MCParticle*, const std::string&,
                           Tuples::Tuple& ) override = 0;

protected:
  void fetchLHCbIDsFromMCParticle( const LHCb::MCParticle& mcp, const LHCb::LinksByKey* mc2IdLink,
                                   WeightedIDsByMCPTrack& weighted_ids_by_mcp_track );
  bool match_to_true_signal( const IDsByTrack& candidate_IDs, const WeightedIDsByMCPTrack& mcp_ID_map );
  bool compileTriggerLineList( const std::vector<std::string>& );

protected:
  DataObjectReadHandle<LHCb::LinksByKey>    m_mc2IdLinkDh = {this, "MC2IDLink",
                                                          "Link/Pr/LHCbID"}; // FIXME is there a default?
  Gaudi::Property<float>                    m_match_threshold{this, "match_threshold", {0.70}};
  std::vector<std::string>                  m_hlt1_line_names{std::vector<std::string>( 0 )};
  std::vector<std::string>                  m_hlt2_line_names{std::vector<std::string>( 0 )};
  Gaudi::Property<std::vector<std::string>> m_triggerList{
      this, "TriggerList", std::vector<std::string>( 0 )}; // property: list of triggers to specifically look at
};
#endif // MCTUPLETOOLTRUESIMEFFBASE_H
