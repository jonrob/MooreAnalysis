/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "boost/regex.hpp"
#include "gsl/gsl_sys.h"

// local
#include "Event/MCParticle.h"
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"
#include "MCTupleToolTrueSimEffHLT1.h"

using namespace LHCb;
//-----------------------------------------------------------------------------
// Implementation file for class : MCTupleToolTrueSimEffHLT1
//
// 2021-09-02 : Ross Hunter
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( MCTupleToolTrueSimEffHLT1 )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MCTupleToolTrueSimEffHLT1::MCTupleToolTrueSimEffHLT1( const std::string& type, const std::string& name,
                                                      const IInterface* parent )
    : MCTupleToolTrueSimEffBase( type, name, parent ) {
  declareInterface<IMCParticleTupleTool>( this );
}

std::map<IDsByTrack, int>
MCTupleToolTrueSimEffHLT1::getLHCbIDsByTrigCand( const LHCb::HltObjectSummary& topLevelSummary ) {

  std::map<IDsByTrack, int> idsByTriggerCandidateMap{};

  if ( topLevelSummary.summarizedObjectCLID() == 1 ) {
    // A selection summary, made up of tracks and vertices
    const SmartRefVector<LHCb::HltObjectSummary>& sub = topLevelSummary.substructure();

    int iCand = 0;
    for ( const auto& subsummary : sub ) {
      IDsByTrack thisCandIDs{};
      if ( !fetchLHCbIDsFromHOS( subsummary, thisCandIDs ) ) {
        error() << "Failed to fetch LHCbIDs from subsummary with CLID " << subsummary->summarizedObjectCLID() << endmsg;
      }
      auto result = idsByTriggerCandidateMap.insert( std::pair<IDsByTrack, int>( thisCandIDs, iCand ) );
      if ( !result.second ) { warning() << "Insertion failed. Is this candidate is already there?" << endmsg; }
      iCand++;
    }
  } else {
    error() << "Not a selection summary. Dont know what to do with this object with summarizedObjectCLID "
            << topLevelSummary.summarizedObjectCLID() << endmsg;
  }
  return idsByTriggerCandidateMap;
}

bool MCTupleToolTrueSimEffHLT1::fetchLHCbIDsFromHOS( const SmartRef<LHCb::HltObjectSummary>& hltObjectSummary,
                                                     IDsByTrack&                             bucket_of_ids ) {
  // Recursive function to iterate over each track of the trigger candidate and collect the LHCbIDs in bucket_of_ids

  const auto& sub = hltObjectSummary->substructure();

  if ( hltObjectSummary->summarizedObjectCLID() == 10030 ) {
    // Vertex-like summary -> should be made of tracks and further vertices -> pass on the substructure...
    bool isEmptySummary = true;
    for ( const auto& subsummary : sub ) {
      if ( !fetchLHCbIDsFromHOS( subsummary, bucket_of_ids ) ) {
        error() << "Failed to fetch LHCbIDs from subsummary with CLID " << subsummary->summarizedObjectCLID() << endmsg;
        return false;
      }
      isEmptySummary = false;
    }
    if ( isEmptySummary ) { return false; }
  } else if ( hltObjectSummary->summarizedObjectCLID() == 10010 ) {
    // Track-like -> add to the bucket.
    bucket_of_ids.emplace_back( hltObjectSummary->lhcbIDs() );

    // NOTE it seems that the lhcbIDs() and lhcbIDsFlattened() method do the same thing, or the case where they differ
    // is unknown to me so we'll put this to catch when this case arises:
    assert( hltObjectSummary->lhcbIDs() == hltObjectSummary->lhcbIDsFlattened() );
  } else {
    warning() << "Dont know how to handle object with summarizedObjectCLID " << hltObjectSummary->summarizedObjectCLID()
              << endmsg;
    return false;
  }
  return true;
}

//=============================================================================
// Fill
//=============================================================================
StatusCode MCTupleToolTrueSimEffHLT1::fill( const LHCb::MCParticle*, const LHCb::MCParticle* mcp,
                                            const std::string& head, Tuples::Tuple& tuple ) {
  const std::string prefix = fullName( head );

  // TODO need to figure out why it looks like this is all getting called twice.
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Filling HLT1 DecisionTrueSim branches for mcp with ID " << mcp->particleID() << endmsg;
  }

  if ( mcp == nullptr ) {
    warning() << "Particle is NULL. Exiting." << endmsg;
    return StatusCode::FAILURE;
  }

  // Get the LHCbIDs from the MCParticle
  LHCb::LinksByKey* mcp_id_link = m_mc2IdLinkDh.get();

  // TODO is weight always =1? If so a simpler vector will suffice.
  WeightedIDsByMCPTrack weighted_mcp_IDs{};
  fetchLHCbIDsFromMCParticle( *mcp, mcp_id_link, weighted_mcp_IDs );

  LHCb::HltSelReports* selReports = m_hltSelReportDh.get();

  for ( const auto& decision_name : m_hlt1_line_names ) {
    // individual Hlt trigger lines
    bool matched_on_this_line{false};

    for ( LHCb::HltSelReports::Container::const_iterator it = selReports->begin(); it != selReports->end(); ++it ) {
      if ( it->first == decision_name ) {

        const LHCb::HltObjectSummary hltObjectSummary  = it->second;
        auto                         idsByCandidateMap = getLHCbIDsByTrigCand( hltObjectSummary );
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << "Found " << idsByCandidateMap.size() << " candidates that the trigger built for " << decision_name
                  << endmsg;
        }

        for ( auto const& [trig_cand_IDs, i] : idsByCandidateMap ) {
          matched_on_this_line |= match_to_true_signal( trig_cand_IDs, weighted_mcp_IDs );
        }
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << "For trigger with name = " << decision_name << ", TrueSim decision = " << matched_on_this_line
                  << " on MCParticle with ID " << mcp->particleID() << endmsg;
        }
      } // If SelRep corresponds to trigger line
    }   // Loop over SelReports
    if ( !tuple->column( prefix + "_" + decision_name + "TrueSim", matched_on_this_line ) ) {
      return StatusCode::FAILURE;
    }
  } // Loop over trigger lines

  if ( msgLevel( MSG::DEBUG ) ) { debug() << "Filled branches for " << prefix << " Hlt1" << endmsg; }

  return StatusCode::SUCCESS;
}
