/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
// from Gaudi
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Event/MCParticle.h"
#include "Event/ProtoParticle.h"
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "Kernel/IMCParticleTupleTool.h"
#include "Kernel/Particle2MCLinker.h"
#include "MCInterfaces/IMCReconstructed.h"
#include "MCInterfaces/IMCReconstructible.h"

using namespace LHCb;
//-----------------------------------------------------------------------------
// Implementation file for class : MCTupleToolReconstructed
//
// 2009-01-19 : Patrick Koppenburg
//-----------------------------------------------------------------------------

/** @class MCTupleToolReconstructed MCTupleToolReconstructed.h
 *
 *  Fill MC Particle
 *
 * See the properties inline documentation to toggle on/off some of
 * the columns
 *
 * - head_Reconstructible : IMCReconstructible category
 * - head_Reconstructed : IMCReconstructed  category
 * - head_PP_PX,Y,Z : ProtoParticle PX, PY, PZ
 * - head_PP_Weight : ProtoParticle association weight
 *
 * \sa MCDecayTreeTuple, IMCReconstructible, IMCReconstructed
 *
 *  @author Patrick Koppenburg
 *  @date   2009-01-22
 */

class MCTupleToolReconstructed : public TupleToolBase, virtual public IMCParticleTupleTool {
public:
  /// Standard constructor
  MCTupleToolReconstructed( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;

public:
  StatusCode fill( const LHCb::MCParticle*, const LHCb::MCParticle*, const std::string&, Tuples::Tuple& ) override;

#include "DecayTreeTupleBase/isStable.h"

private:
  IMCReconstructible*   m_recible = nullptr;                                                   ///< Reconstructible tool
  IMCReconstructed*     m_rected  = nullptr;                                                   ///< Reconstructed too
  Gaudi::Property<bool> m_associate{this, "Associate", true, "Fill associated protoparticle"}; ///< Do association
  Gaudi::Property<bool> m_pid{this, "FillPID", true, "Fill PID, also set by Verbose"};         ///< Do PID
};

// Declaration of the Tool Factory
DECLARE_COMPONENT( MCTupleToolReconstructed )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MCTupleToolReconstructed::MCTupleToolReconstructed( const std::string& type, const std::string& name,
                                                    const IInterface* parent )
    : TupleToolBase( type, name, parent ) {
  declareInterface<IMCParticleTupleTool>( this );
}

//=============================================================================
// initialize
//=============================================================================

StatusCode MCTupleToolReconstructed::initialize() {
  return TupleToolBase::initialize().andThen( [&] {
    m_recible = tool<IMCReconstructible>( "MCReconstructible" );
    m_rected  = tool<IMCReconstructed>( "MCReconstructed" );

    if ( isVerbose() ) m_pid = true;
  } );
}
//=============================================================================
// Fill
//=============================================================================
StatusCode MCTupleToolReconstructed::fill( const LHCb::MCParticle*, const LHCb::MCParticle* mcp,
                                           const std::string& head, Tuples::Tuple& tuple ) {

  struct Asct {
    GaudiTool*                                                   self      = nullptr;
    std::optional<Object2FromMC<LHCb::ProtoParticle, GaudiTool>> m_charged = {};
    std::optional<Object2FromMC<LHCb::ProtoParticle, GaudiTool>> m_neutral = {};
    auto                                                         operator()( const LHCb::MCParticle* p ) {
      if ( p->particleID().threeCharge() == 0 ) {
        if ( !m_neutral )
          m_neutral.emplace( self, Particle2MCMethod::NeutralPP, LHCb::ProtoParticleLocation::Neutrals );
        return m_neutral->range( p );
      } else {
        if ( !m_charged ) m_charged.emplace( self, Particle2MCMethod::ChargedPP, LHCb::ProtoParticleLocation::Charged );
        return m_charged->range( p );
      }
    }
  };

  Asct asct{this};

  const std::string prefix = fullName( head );

  bool test    = true;
  int  catted  = -10;
  int  catible = -10;

  // pointer is ready, prepare the values:
  if ( mcp ) {
    catted  = m_rected->reconstructed( mcp );
    catible = m_recible->reconstructible( mcp );
  }

  // fill the tuple:
  test &= tuple->column( prefix + "_Reconstructible", catible );
  test &= tuple->column( prefix + "_Reconstructed", catted );
  std::vector<double> PX, PY, PZ, Weights, dlle, dllmu, dllk, dllp, pchi2;

  if ( ( 0 != mcp ) && m_associate && isStable( mcp ) ) {
    for ( const auto& [proto, w] : asct( mcp ) ) {
      /// @todo There's plenty more that can be added here. Like PID for instance.
      if ( proto.track() ) {
        Gaudi::XYZVector mom = proto.track()->momentum();
        PX.push_back( mom.X() );
        PY.push_back( mom.Y() );
        PZ.push_back( mom.Z() );
        pchi2.push_back( proto.track()->probChi2() );
      }
      Weights.push_back( w );
      if ( m_pid ) {
        dlle.push_back( proto.info( ProtoParticle::CombDLLe, -999.0 ) );
        dllmu.push_back( proto.info( ProtoParticle::CombDLLmu, -999.0 ) );
        dllk.push_back( proto.info( ProtoParticle::CombDLLk, -999.0 ) );
        dllp.push_back( proto.info( ProtoParticle::CombDLLp, -999.0 ) );
      }
    }
  }
  const unsigned int maxPP = 20;
  test &= tuple->farray( prefix + "_PP_PX", PX, prefix + "_ProtoParticles", maxPP );
  test &= tuple->farray( prefix + "_PP_PY", PY, prefix + "_ProtoParticles", maxPP );
  test &= tuple->farray( prefix + "_PP_PZ", PZ, prefix + "_ProtoParticles", maxPP );
  test &= tuple->farray( prefix + "_PP_Weight", Weights, prefix + "_ProtoParticles", maxPP );
  test &= tuple->farray( prefix + "_PP_tr_pchi2", pchi2, prefix + "_ProtoParticles", maxPP );
  if ( m_pid ) {
    test &= tuple->farray( prefix + "_PP_DLLe", dlle, prefix + "_ProtoParticles", maxPP );
    test &= tuple->farray( prefix + "_PP_DLLk", dllk, prefix + "_ProtoParticles", maxPP );
    test &= tuple->farray( prefix + "_PP_DLLp", dllp, prefix + "_ProtoParticles", maxPP );
    test &= tuple->farray( prefix + "_PP_DLLmu", dllmu, prefix + "_ProtoParticles", maxPP );
  }

  return StatusCode( test );
}
